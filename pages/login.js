import React from "react"
import IconButton from "@material-ui/core/IconButton";
import { useRouter} from "next/router";
import ArrowBackIosRoundedIcon from "@material-ui/icons/ArrowBackIosRounded";
import Typography from "@material-ui/core/Typography"


function login() {
    const router = useRouter()
    return(
        <div>
            <div class ="MuiContainer-root jss45 MuiContainer-maxWidthXs" style={{
                height:"100%",
                padding:0,
                maxWidth:"444px",
                marginTop:"-16px",
                minHeight:"100vh",
                borderLeft:"1px solid #f1f1f1",
                paddingTop:"64px",
                borderRight:"1px solid #f1f1f1",
                backgroundColor:"#fff",
                marginLeft:"auto",
                marginRight:"auto"
            }}>
                <div class="MuiBox-root jss230 jss221 jss210" id="box" style={{
                    top:0,
                    color:"white",
                    width:"100%",
                    zIndex:99,
                    position:"fixed",
                    maxWidth:"442px",
                    background:"white"
                }}>
                    <header class="MuiPaper-root MuiAppBar-root MuiAppBar-positionStatic MuiAppBar-colorPrimary jss220 jss209 MuiPaper-elevation0" style={{
                        backgroundcolor: "white",
                         color: "black",
                         width:"100%",
                         zIndex:999,
                         position:"fixed",
                         maxWidth:"442px",
                         boxShadow:"0px 1px 5px rgb(0 0 0 / 5%)"
                    }}>
                     <div class="MuiToolbar-root MuiToolbar-dense MuiToolbar-gutters" style={{
                         paddingRight:"16px",
                         paddingRight:"16px",
                         minHeight:"60px",
                         display:"flex",
                         position:"relative",
                         alignItems:"center",

                     }}>
                      <IconButton
           onClick={()=>router.push("/")}
              edge="start"
              style={{
                color: "#F4783B",
                marginRight:0,
                marginLeft:"3px"
              }}
            >
              <ArrowBackIosRoundedIcon />
            </IconButton>
            <Typography
              color="inherit"
              style={{
             fontWeight:700,
             fontFamily:"Montserrat,sans-serif"
           
              }}
            >
             <strong style={{
                 fontFamily:"Montserrat,sans-serif"
             }}>Masuk Akun</strong>
            </Typography>
                     </div>
                    </header>
                </div>
                <div style={{
                    marginBottom:"20px"
                }}>
                    <div>
                        <div class="firebaseui-container firebaseui-page-provider-sign-in firebaseui-id-page-provider-sign-in firebaseui-use-spinner" style={{
                            background:"inherit",
                            WebkitBoxSizing:"border-box",
                            color:"rgba(0,0,0,.87)",
                            flexDirection:"1tr",
                           margin:"0 auto",
                           boxShadow:"none",
                           maxWidth:"none",
                           width:"100%",
                            
                        }}>
                            <div class="firebaseui-card-content" style={{
                                padding:"0 24px"
                            }}>
                       <form>
                           <ul class="firebaseui-idp-list" style={{
                               listStyle:"none",
                               margin:"1em 0",
                               padding:0
                           }}>
                            <div class="MuiGrid-root MuiGrid-container" align="center" style={{
                                width:"100%",
                                display:"flex",
                                flexWrap:"wrap",
                                boxSizing:"border-box"
                            }}>
                                <div class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-12" style={{
                                    marginTop: "75px", 
                                    margintop: "72px",
                                    flexGrow:0,
                                    maxWidth:"100%",
                                    flexBasis:"100%"
                                }}>
                                    <img src="https://goodpangan.com/static/media/masuk.59fdce38.svg" alt="Masuk" >
                                    </img>
                                </div>
                                <div class="MuiGrid-root jss181 MuiGrid-item MuiGrid-grid-xs-12" style={{
                                    marginTop:"37px",
                                    maxWidth:"100%",
                                    flexBasis:"100%"
                                }}>
                                    <Typography class="MuiTypography-root MuiTypography-body2" style={{
                                        fontSize:"18px",
                                        fontWeight:600,
                                        lineHeight:1.43,
                                        fontFamily:"Montserrat,sans-serif"
                                    }}>
                                  <b style={{
                                      fontFamily:"Montserrat,sans-serif"
                                  }}>Masuk</b>
                                    </Typography>
                                 <span class="MuiTypography-root MuiTypography-caption MuiTypography-displayBlock" style={{
                                     display:"block",
                                     fontWeight:400,
                                     lineHeight:1.66,
                                     margin:0,
                                     fontSize:"14px"
                                 }}>
                                  Nikmati kepuasan dan kenyamanan kualitas belanja kebutuhan sehari - hari dengan GoodPangan
                                 </span>
                                </div>
                                <div class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-12" style={{
                                    flexGrow:0,
                                    maxWidth:"100%",
                                    flexBasis:"100%",
                                    marginTop:"10%"
                                }}>
                                  
                              <button  
                                 onClick={()=>router.push("/profile")}
                              data-provider-id="google.com" data-upgraded=",MaterialButton" style={{
                                  backgroundColor:"rgb(66, 133, 244)",
                                  margin:0,
                                  padding:"3px",
                                  borderRadius:"4px",
                                  border:"none",
                                  direction:"ltr",
                                  fontWeight:500,
                                  lineHeight:"normal",
                                  maxWidth:"220px",
                                  minHeight:"40px",
                                  textAlign:"left",
                                  width:"100%"
                              }}>
                                  
                                  <span align="center" class="firebaseui-idp-icon-wrapper" style={{
                                   backgroundColor:"white",
                                   borderRadius:"4px",
                                   width:"20%",
                                   margin:0,
                                   display:"table-cell",
                                   verticalAlign:"middle"

                                  }}>
                                      
                                      <img class="firebaseui-idp-icon" src="https://goodpangan.com/static/media/google.1afb8f6b.svg" style={{
                                          width:"37px",
                                          height:"37px",
                                          border:"none",
                                          display:"inline-block",
                                          verticalAlign:"middle"
                                      }}>
                                       
                                      </img>
                                      
                                      </span>
                                      
                                      <span    
                                      
                                     edge="start" class="firebaseui-idp-text firebaseui-idp-text-long" style={{
                                          color:"white",
                                          fontSize:"14px",
                                          display:"table-cell",
                                          paddingLeft:"15px",
                                          tetxtransform:"none",
                                          verticalAlign:"middle",
                                          boxSizing:"inherit",
                                          textAlign:"left",
                                          cursor:"pointer",
                                          direction:"1tr",
                                           fontWeight:500,
                                           lineHeight:"normal",
                                           fontWeight:700,
                                           color:"white",
                                           fontSize:"14px",
                                           fontFamily:"Montserrat,sans-serif"
                                      }}><b style={{
                                        fontFamily:"Montserrat,sans-serif"
                                      }}>Masuk Dengan Google</b></span>
                              </button>
                              
                                </div>
                                <div class="MuiGrid-root jss602 MuiGrid-item MuiGrid-grid-xs-12" style={{
                                    marginTop:"40px",
                                    flexGrow:0,
                                    maxWidth:"100%",
                                    flexBasis:"100%",
                                    margin:0,
                                    boxSizing:"border-box"
                                }}>
                                    
                                    <button  class="firebaseui-idp-button mdl-button mdl-js-button mdl-button--raised firebaseui-idp-password firebaseui-id-idp-button"
                                    data-provider-id="password" data-upgraded=",MaterialButton" style={{
                                        backgroundColor:"rgb(244, 120, 59)",
                                        margin:0,
                                        padding:"3px",
                                        borderRadius:"4px",
                                        direction:"1tr",
                                        fontWeight:500,
                                        height:"auto",
                                        lineHeight:"normal",
                                        maxWidth:"220px",
                                        minHeight:"40px",
                                        textAlign:"text",
                                        width:"100%",
                                        border:"none",
                                        marginTop:"10px"
                                    }}>
                                        <span align="center" class="firebaseui-idp-icon-wrapper" style={{
                                            backgroundColor:"white",
                                            borderRadius:"4px",
                                            width:"23%",
                                            margin:0,
                                            display:"table-cell",
                                            verticalAlign:"middle",
                                            boxSizing:"inherit",
                                            direction:"1tr",
                                            fontWeight:500,
                                            lineHeight:"nomal"
                                        }}>
                                            <img class ="firebaseui-idp-icon" src="https://goodpangan.com/static/media/emaillain.8227a19c.svg" style={{
                                                width:"25px",
                                                height:"35px",
                                                border:"none",
                                                display:"inline-block",
                                                verticalAlign:"middle",
                                                direction:"1tr",
                                                fontWeight:500,
                                                lineHeight:"nomal",
                                                textAlign:"left"
                                            }}></img>
                                        </span>
                                        <span class="firebaseui-idp-text firebaseui-idp-text-long" style={{
                                            fontSize:"14px",
                                            display:"table-cell",
                                            color:"#fff",
                                            paddingLeft:"15px",
                                            transform:"none",
                                            verticalAlign:"middle",
                                            direction:"1tr",
                                                fontWeight:500,
                                                lineHeight:"nomal",
                                                textAlign:"left",
                                                fontWeight:700
                                        }}><b>Daftar Lewat Email</b></span>
                                    </button>
                                                  </div>
                                </div>
                           </ul>
                       </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="MuiBox-root jss862 " style={{
                fontSize:"6px",
                display:"block",
                boxSizing:"inherit",

            }}>
                <span class="MuiTypography-root MuiTypography-caption MuiTypography-gutterBottom 
                MuiTypography-alignCenter MuiTypography-displayBlock" style={{
                    color:"rgb(159, 163, 166)",
                    fontSize:"11px",
                    display:"block",
                    textAlign:"center",
                    fontWeight:400,
                    lineHeight:1.66,
                    margin:0,
                    marginTop:"10px",
                }}>
                 Dengan masuk dan mendaftar, Anda menyetujui
                </span>
                <span class="MuiTypography-root MuiTypography-caption MuiTypography-gutterBottom MuiTypography-alignCenter MuiTypography-displayBlock"style={{
                    color:"rgb(159, 163, 166)",
                    fontWeight:"bold",
                    fontSize:"11px",
                    display:"block",
                    paddingBottom:"0.35em",
                    textAlign:"center",
                    lineHeight:1.66,
                    margin:0
                }}>
                    <u style="cursor: pointer:" style={{
                        cursor:"pointer",
                        boxSizing:"inherit",
                        color:"rgb(159, 163, 166)",
                        fontSize:"11px",
                        fontWeight:"bold",
                        textAlign:"center"
                    }}>Syarat Penggunaan</u>          
                    &nbsp; dan &nbsp;
                    <u style="cursor: pointer;" style={{
                        cursor:"pointer",
                        boxSizing:"inherit",
                        color:"rgb(159, 163, 166)",
                        fontSize:"11px",
                        fontWeight:"bold",
                        textAlign:"center"
                    }}>Kebijakan Privasi</u>
               
                </span>
                </div>
                
            </div>
         
            </div>
        
    )
}
export default login;