import React from "react"
import IconButton from "@material-ui/core/IconButton";
import { useRouter} from "next/router";
import ArrowBackIosRoundedIcon from "@material-ui/icons/ArrowBackIosRounded";
import { Typography } from "@material-ui/core";

const produk=[
    {
        name:"Clutser Semarang",
        category:"Produk terbaik di pasar ini",
        lokasi:"Alamat :"
    },
    {
        name:"Clutser Batang",
        category:"Produk terbaik di pasar ini",
        lokasi:"Alamat :"
    },
    {
        name:"Clutser Magelang",
        category:"Produk terbaik di pasar ini",
        lokasi:"Alamat"
    },
    {
        name:"Clutser Kendal",
        category:"Produk terbaik di pasar ini",
        lokasi:"Alamat :"
    },
    {
        name:"Clutser Kabupaten Semarang",
        category:"Produk terbaik di pasar ini",
        lokasi:"Alamat :"
    },
    {
        name:"Clutser Sragen",
        category:"Produk terbaik di pasar ini",
        lokasi:"Alamat :"
    }
]
function Market() {
    const router = useRouter()
    return(
        <div>
            <div class="MuiContainer-root jss426 MuiContainer-maxWidthXs" style={{
                height:"100vh",
                padding:0,
                background:"#FBFDFF",
                borderLeft:"1px solid #f1f1f1",
                borderRight:"1px solid #f1f1f1",
                maxWidth:"444px",
                width:"100%",
                display:"block",
                marginLeft:"auto",
                marginRight:"auto",

            }}>
                <header class="MuiPaper-root MuiAppBar-root MuiAppBar-positionStatic MuiAppBar-colorPrimary jss427 MuiPaper-elevation0" style={{
                    top:0,
                    width:"100%",
                    zIndex:0,
                    pointerEvents:"fixed",
                    maxWidth:"442px",
                    background:"white",
                    boxShadow:"0px 1px 5px rgb(0 0 0 / 5%)",
                    minHeight:"120px"
                }}>
                    <div class="MuiToolbar-root MuiToolbar-dense MuiToolbar-gutters" style={{
                        minHeight:"48px",
                        paddingLeft:"16px",
                        paddingRight:"16px",
                        display:"flex",
                        position:"relative",
                        alignItems:"center",
                        boxSizing:"inherit", 
                    }}>
                        <div class="MuiGrid-root MuiGrid-container" style={{
                            display: "flex",
                            flexDirection:"row",
                           alignItems:"center",
                           width:"100%",
                           flexWrap:"wrap",
                           boxSizing:"border-box",

                        }}>
                            <div class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-1" style={{
                                maxWidth:"8.333333%",
                                flexBasis:"8.333333%",
                                margin:0,
                                boxSizing:"border-box",
                                display:"block",
                            }}>
                                 <IconButton
           onClick={()=>router.push("/")}
              edge="start"
              style={{
                color: "#F4783B",
                width:"100%",
                marginRight:0,
               textAlign:"left",
               marginRight:"50px"
              
              }}
            >
               
              <ArrowBackIosRoundedIcon style={{
                   fontSize:"1.2em",
              }}/>
            </IconButton> 
                              </div>
                             <Typography class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-11" style={{
                                 color:"black",
                                 paddingRight:0,
                                 fontSize:"1.1rem",
                                 fontWeight:"bold",
                                 lineHeight:1.3,
                                 margin:0,
                                 marginLeft:"-8px",
                                 
                             }}>Pilih Lokasi Pasar</Typography>
                              </div>

                                  </div>
                                  <div class="MuiGrid-root MuiGrid-container" style={{
                                  width:"100%",
                                  display:"flex",
                                  flexWrap:"wrap",
                                  boxSizing:"border-box",
                                  color:"rgba(0, 0, 0, 0.87)",
                                  fontWeight:100,
                                  lineHeight:1.43,
                                  fontFamily: "'Montserrat', sans-serif",
                                  
                              }}>
                                  <div class="MuiGrid-root jss361 MuiGrid-item MuiGrid-grid-xs-12" style={{
                                      display:"flex",
                                      padding:"2px",
                                      background:"#F2F2F2",
                                      marginTop:"3%",
                                      alignItems:"center",
                                      borderRadius:"100px",
                                      marginBottom:"10px",
                                      flexGrow:0,
                                      maxWidth:"90%",
                                      width:"100%",
                                      marginLeft:"20px",
                                      paddingLeft:"17px",
                                      flexBasis:"100%"
                                  }}>
                                      <svg class="MuiSvgIcon-root" focusable="false" viewBox="0 0 24 24" aria-hidden="true" style={{
                                          fill:"currentcolor",
                                          color:"rgb(112, 117, 133)",
                                          width:"1em",
                                          height:"1.5em",
                                          alignItems:"center",
                                          display:"inline-block",
                                          fontSize:"1.5rem",
                                          transition:"fill 200ms cubic-bezier(0.4, 0, 0.2, 1) 0ms",
                                          flexShrink:0,
                                          userSelect:"none",
                                          overflow:"hidden",
                                          boxSizing:"inherit",
                                          fontWeight:400,
                                          lineHeight:1.43,
                                      }}>
                        <path d="M15.5 14h-.79l-.28-.27C15.41 12.59 16 11.11 16 9.5 16 5.91 13.09 3 9.5 3S3 5.91 3 9.5 5.91 16 9.5 16c1.61 0 3.09-.59 4.23-1.57l.27.28v.79l5 4.99L20.49 19l-4.99-5zm-6 0C7.01 14 5 11.99 5 9.5S7.01 5 9.5 5 14 7.01 14 9.5 11.99 14 9.5 14z" style={{
                            
                        }}></path>
                                      </svg>
                                      <div class="MuiInputBase-root jss49" style={{
                                          color:"#707585",
                                          width:"90%",
                                          fontSize:"12px",
                                          paddingLeft:"2%",
                                          cursor:"text",
                                          display:"inline-flex",
                                          position:"relative",
                                          boxSizing:"border-box",
                                          alignItems:"center",
                                          fontWeight:400,
                                          lineHeight:"1.1876em"
                                      }}>
                                          <input placeholder="Pasar apa yang kamu cari ?" type="text" class="MuiInputBase-input"  style={{
                                              font:"inherit",
                                              color:"currentcolor",
                                              width:"100%",
                                              border:0,
                                              height:"1.1876em",
                                              margin:0,
                                              display:"block",
                                              padding:"6px 0 7px",
                                              minWidth:0,
                                              background:"none",
                                              boxSizing:"content-box",
                                              animationName:"mui-auto-fill-cancel",
                                              letterSpacing:"inherit",
                                              animationDuration:"10ms",
                                              WebkitTapHighlightColor:"transparent"
                                          }}></input>
                                      </div>
                              </div>
                    </div>
                </header>
                <div class="MuiGrid-root jss52 MuiGrid-container" style={{
                    marginTop:"13px",
                    width:"100%",
                    display:"flex",
                    flexWrap:"wrap",
                    boxSizing:"border-box",
                }}>
                    <div class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-12" style={{
                        flexGrow:0,
                        maxWidth:"100%",
                        flexBasis:"100%",
                        margin:0,
                        boxSizing:"border-box",
                        display:"block",
                    }}>
                        <div class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-12" style={{
                        flexGrow:0,
                        maxWidth:"100%",
                        flexBasis:"100%",
                        margin:0,
                        boxSizing:"border-box",
                        display:"block",
                    }}>
                        <div style={{
                            padding:"12px 16px",
                            boxSizing:"inherit",
                            display:"block"
                        }}>
                            <div class="MuiGrid-root jss60 MuiGrid-item MuiGrid-grid-xs-12" style={{
                                display:"flex",
                                justifyContent:"Space-Between",
                                flexGrow:0,
                                flexBasis:"100%",
                                maxWidth:"100%",
                                margin:0,
                                boxSizing:"border-box"
                            }}>
                                <Typography class="MuiTypography-root MuiTypography-body1" style={{
                                    fontSize:"15px",
                                    fontWeight:400,
                                    lineHeight:1.5,
                                    margin:0,
                                    boxSizing:"inherit",
                                    display:"block",
                                    marginBlockStart:"1em",
                                    marginBlockEnd:"1em",
                                    marginInlineEnd:"0px",
                                    marginInlineStart:"0px",
                                    marginBottom:"5px"
                                }}><b>Clutser Semarang</b></Typography>
                                <img src="https://goodpangan.com//static/media/badge.dda219fb.svg">

                                </img>
                            </div>
                            <div class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-12" style={{
                                flexGrow:0,
                                maxWidth:"!00%",
                                flexBasis:"100%",
                                margin:0,
                                boxSizing:"border-box",
                                
                            }}>
                                <Typography class="MuiTypography-root jss62 MuiTypography-body1" style={{
                                    paddingTop:0,
                                    width:"100%",
                                    fontSize:"13px",
                                    fontWeight:400,
                                    lineHeight:1.5,
                                    margin:0,
                                    boxSizing:"inherit"
                                }}>Produk terbaik pasar ini:</Typography>
                                <Typography class="MuiTypography-root MuiTypography-body1" style={{
                                    color:"rgb(112, 117, 133)",
                                    fontSize:"10px",
                                    fontWeight:400,
                                    lineHeight:1.5,
                                    margin:0
                                }}></Typography>
                                <Typography class="MuiTypography-root MuiTypography-body1" style={{
                                    fontSize:"13px",
                                    paddingTop:"2%",
                                    fontWeight:400,
                                    lineHeight:1.5
                                }}> Alamat :</Typography>
                                <Typography class="MuiTypography-root MuiTypography-body1" style={{
                                    color:"rgb(112, 117, 133)",
                                    fontSize:"10px",
                                    fontWeight:400,
                                    lineHeight:1.5,
                                    margin:0
                                }}></Typography>
                            </div>
                            </div>
                            <hr class="MuiDivider-root" style={{
                                border:"none",
                                height:"1px",
                                margin:0,
                                flexShrink:0,
                                backgroundColor:"rgba(0, 0, 0, 0.12)",
                                boxSizing:"inherit",
                                display:"block",
                                unicodeBidi:"isolate",
                                marginBlockStart:"0.5em",
                                marginBlockEnd:"0.5em",
                                marginInlineStart:"auto",
                                marginInlineEnd:"auto",
                            }}></hr>
                    </div>
                    </div>
                    <div class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-12" style={{
                        flexGrow:0,
                        maxWidth:"100%",
                        flexBasis:"100%",
                        margin:0,
                        boxSizing:"border-box",
                        display:"block",
                    }}>
                        <div class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-12" style={{
                        flexGrow:0,
                        maxWidth:"100%",
                        flexBasis:"100%",
                        margin:0,
                        boxSizing:"border-box",
                        display:"block",
                    }}>
                        <div style={{
                            padding:"12px 16px",
                            boxSizing:"inherit",
                            display:"block"
                        }}>
                            <div class="MuiGrid-root jss60 MuiGrid-item MuiGrid-grid-xs-12" style={{
                                display:"flex",
                                justifyContent:"Space-Between",
                                flexGrow:0,
                                flexBasis:"100%",
                                maxWidth:"100%",
                                margin:0,
                                boxSizing:"border-box"
                            }}>
                                <Typography class="MuiTypography-root MuiTypography-body1" style={{
                                    fontSize:"15px",
                                    fontWeight:400,
                                    lineHeight:1.5,
                                    margin:0,
                                    boxSizing:"inherit",
                                    display:"block",
                                    marginBlockStart:"1em",
                                    marginBlockEnd:"1em",
                                    marginInlineEnd:"0px",
                                    marginInlineStart:"0px",
                                    marginBottom:"5px"
                                }}><b>Clutser Batang</b></Typography>
                                <img src="https://goodpangan.com//static/media/badge.dda219fb.svg">

                                </img>
                            </div>
                            <div class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-12" style={{
                                flexGrow:0,
                                maxWidth:"!00%",
                                flexBasis:"100%",
                                margin:0,
                                boxSizing:"border-box",
                                
                            }}>
                                <Typography class="MuiTypography-root jss62 MuiTypography-body1" style={{
                                    paddingTop:0,
                                    width:"100%",
                                    fontSize:"13px",
                                    fontWeight:400,
                                    lineHeight:1.5,
                                    margin:0,
                                    boxSizing:"inherit"
                                }}>Produk terbaik pasar ini:</Typography>
                                <Typography class="MuiTypography-root MuiTypography-body1" style={{
                                    color:"rgb(112, 117, 133)",
                                    fontSize:"10px",
                                    fontWeight:400,
                                    lineHeight:1.5,
                                    margin:0
                                }}></Typography>
                                <Typography class="MuiTypography-root MuiTypography-body1" style={{
                                    fontSize:"13px",
                                    paddingTop:"2%",
                                    fontWeight:400,
                                    lineHeight:1.5
                                }}> Alamat :</Typography>
                                <Typography class="MuiTypography-root MuiTypography-body1" style={{
                                    color:"rgb(112, 117, 133)",
                                    fontSize:"10px",
                                    fontWeight:400,
                                    lineHeight:1.5,
                                    margin:0
                                }}></Typography>
                            </div>
                            </div>
                            <hr class="MuiDivider-root" style={{
                                border:"none",
                                height:"1px",
                                margin:0,
                                flexShrink:0,
                                backgroundColor:"rgba(0, 0, 0, 0.12)",
                                boxSizing:"inherit",
                                display:"block",
                                unicodeBidi:"isolate",
                                marginBlockStart:"0.5em",
                                marginBlockEnd:"0.5em",
                                marginInlineStart:"auto",
                                marginInlineEnd:"auto",
                            }}></hr>
                    </div>
                    </div>
                    <div class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-12" style={{
                        flexGrow:0,
                        maxWidth:"100%",
                        flexBasis:"100%",
                        margin:0,
                        boxSizing:"border-box",
                        display:"block",
                    }}>
                        <div class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-12" style={{
                        flexGrow:0,
                        maxWidth:"100%",
                        flexBasis:"100%",
                        margin:0,
                        boxSizing:"border-box",
                        display:"block",
                    }}>
                        <div style={{
                            padding:"12px 16px",
                            boxSizing:"inherit",
                            display:"block"
                        }}>
                            <div class="MuiGrid-root jss60 MuiGrid-item MuiGrid-grid-xs-12" style={{
                                display:"flex",
                                justifyContent:"Space-Between",
                                flexGrow:0,
                                flexBasis:"100%",
                                maxWidth:"100%",
                                margin:0,
                                boxSizing:"border-box"
                            }}>
                                <Typography class="MuiTypography-root MuiTypography-body1" style={{
                                    fontSize:"15px",
                                    fontWeight:400,
                                    lineHeight:1.5,
                                    margin:0,
                                    boxSizing:"inherit",
                                    display:"block",
                                    marginBlockStart:"1em",
                                    marginBlockEnd:"1em",
                                    marginInlineEnd:"0px",
                                    marginInlineStart:"0px",
                                    marginBottom:"5px"
                                }}><b>Cluster Magelang</b></Typography>
                                <img src="https://goodpangan.com//static/media/badge.dda219fb.svg">

                                </img>
                            </div>
                            <div class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-12" style={{
                                flexGrow:0,
                                maxWidth:"!00%",
                                flexBasis:"100%",
                                margin:0,
                                boxSizing:"border-box",
                                
                            }}>
                                <Typography class="MuiTypography-root jss62 MuiTypography-body1" style={{
                                    paddingTop:0,
                                    width:"100%",
                                    fontSize:"13px",
                                    fontWeight:400,
                                    lineHeight:1.5,
                                    margin:0,
                                    boxSizing:"inherit"
                                }}>Produk terbaik pasar ini:</Typography>
                                <Typography class="MuiTypography-root MuiTypography-body1" style={{
                                    color:"rgb(112, 117, 133)",
                                    fontSize:"10px",
                                    fontWeight:400,
                                    lineHeight:1.5,
                                    margin:0
                                }}></Typography>
                                <Typography class="MuiTypography-root MuiTypography-body1" style={{
                                    fontSize:"13px",
                                    paddingTop:"2%",
                                    fontWeight:400,
                                    lineHeight:1.5
                                }}> Alamat :</Typography>
                                <Typography class="MuiTypography-root MuiTypography-body1" style={{
                                    color:"rgb(112, 117, 133)",
                                    fontSize:"10px",
                                    fontWeight:400,
                                    lineHeight:1.5,
                                    margin:0
                                }}></Typography>
                            </div>
                            </div>
                            <hr class="MuiDivider-root" style={{
                                border:"none",
                                height:"1px",
                                margin:0,
                                flexShrink:0,
                                backgroundColor:"rgba(0, 0, 0, 0.12)",
                                boxSizing:"inherit",
                                display:"block",
                                unicodeBidi:"isolate",
                                marginBlockStart:"0.5em",
                                marginBlockEnd:"0.5em",
                                marginInlineStart:"auto",
                                marginInlineEnd:"auto",
                            }}></hr>
                    </div>
                    </div>
                    <div class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-12" style={{
                        flexGrow:0,
                        maxWidth:"100%",
                        flexBasis:"100%",
                        margin:0,
                        boxSizing:"border-box",
                        display:"block",
                    }}>
                        <div class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-12" style={{
                        flexGrow:0,
                        maxWidth:"100%",
                        flexBasis:"100%",
                        margin:0,
                        boxSizing:"border-box",
                        display:"block",
                    }}>
                        <div style={{
                            padding:"12px 16px",
                            boxSizing:"inherit",
                            display:"block"
                        }}>
                            <div class="MuiGrid-root jss60 MuiGrid-item MuiGrid-grid-xs-12" style={{
                                display:"flex",
                                justifyContent:"Space-Between",
                                flexGrow:0,
                                flexBasis:"100%",
                                maxWidth:"100%",
                                margin:0,
                                boxSizing:"border-box"
                            }}>
                                <Typography class="MuiTypography-root MuiTypography-body1" style={{
                                    fontSize:"15px",
                                    fontWeight:400,
                                    lineHeight:1.5,
                                    margin:0,
                                    boxSizing:"inherit",
                                    display:"block",
                                    marginBlockStart:"1em",
                                    marginBlockEnd:"1em",
                                    marginInlineEnd:"0px",
                                    marginInlineStart:"0px",
                                    marginBottom:"5px"
                                }}><b>Cluster Kendal</b></Typography>
                                <img src="https://goodpangan.com//static/media/badge.dda219fb.svg">

                                </img>
                            </div>
                            <div class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-12" style={{
                                flexGrow:0,
                                maxWidth:"!00%",
                                flexBasis:"100%",
                                margin:0,
                                boxSizing:"border-box",
                                
                            }}>
                                <Typography class="MuiTypography-root jss62 MuiTypography-body1" style={{
                                    paddingTop:0,
                                    width:"100%",
                                    fontSize:"13px",
                                    fontWeight:400,
                                    lineHeight:1.5,
                                    margin:0,
                                    boxSizing:"inherit"
                                }}>Produk terbaik pasar ini:</Typography>
                                <Typography class="MuiTypography-root MuiTypography-body1" style={{
                                    color:"rgb(112, 117, 133)",
                                    fontSize:"10px",
                                    fontWeight:400,
                                    lineHeight:1.5,
                                    margin:0
                                }}></Typography>
                                <Typography class="MuiTypography-root MuiTypography-body1" style={{
                                    fontSize:"13px",
                                    paddingTop:"2%",
                                    fontWeight:400,
                                    lineHeight:1.5
                                }}> Alamat :</Typography>
                                <Typography class="MuiTypography-root MuiTypography-body1" style={{
                                    color:"rgb(112, 117, 133)",
                                    fontSize:"10px",
                                    fontWeight:400,
                                    lineHeight:1.5,
                                    margin:0
                                }}></Typography>
                            </div>
                            </div>
                            <hr class="MuiDivider-root" style={{
                                border:"none",
                                height:"1px",
                                margin:0,
                                flexShrink:0,
                                backgroundColor:"rgba(0, 0, 0, 0.12)",
                                boxSizing:"inherit",
                                display:"block",
                                unicodeBidi:"isolate",
                                marginBlockStart:"0.5em",
                                marginBlockEnd:"0.5em",
                                marginInlineStart:"auto",
                                marginInlineEnd:"auto",
                            }}></hr>
                    </div>
                    </div>
                    <div class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-12" style={{
                        flexGrow:0,
                        maxWidth:"100%",
                        flexBasis:"100%",
                        margin:0,
                        boxSizing:"border-box",
                        display:"block",
                    }}>
                        <div class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-12" style={{
                        flexGrow:0,
                        maxWidth:"100%",
                        flexBasis:"100%",
                        margin:0,
                        boxSizing:"border-box",
                        display:"block",
                    }}>
                        <div style={{
                            padding:"12px 16px",
                            boxSizing:"inherit",
                            display:"block"
                        }}>
                            <div class="MuiGrid-root jss60 MuiGrid-item MuiGrid-grid-xs-12" style={{
                                display:"flex",
                                justifyContent:"Space-Between",
                                flexGrow:0,
                                flexBasis:"100%",
                                maxWidth:"100%",
                                margin:0,
                                boxSizing:"border-box"
                            }}>
                                <Typography class="MuiTypography-root MuiTypography-body1" style={{
                                    fontSize:"15px",
                                    fontWeight:400,
                                    lineHeight:1.5,
                                    margin:0,
                                    boxSizing:"inherit",
                                    display:"block",
                                    marginBlockStart:"1em",
                                    marginBlockEnd:"1em",
                                    marginInlineEnd:"0px",
                                    marginInlineStart:"0px",
                                    marginBottom:"5px"
                                }}><b>Cluster Kabupaten Semarang</b></Typography>
                                <img src="https://goodpangan.com//static/media/badge.dda219fb.svg">

                                </img>
                            </div>
                            <div class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-12" style={{
                                flexGrow:0,
                                maxWidth:"!00%",
                                flexBasis:"100%",
                                margin:0,
                                boxSizing:"border-box",
                                
                            }}>
                                <Typography class="MuiTypography-root jss62 MuiTypography-body1" style={{
                                    paddingTop:0,
                                    width:"100%",
                                    fontSize:"13px",
                                    fontWeight:400,
                                    lineHeight:1.5,
                                    margin:0,
                                    boxSizing:"inherit"
                                }}>Produk terbaik pasar ini:</Typography>
                                <Typography class="MuiTypography-root MuiTypography-body1" style={{
                                    color:"rgb(112, 117, 133)",
                                    fontSize:"10px",
                                    fontWeight:400,
                                    lineHeight:1.5,
                                    margin:0
                                }}></Typography>
                                <Typography class="MuiTypography-root MuiTypography-body1" style={{
                                    fontSize:"13px",
                                    paddingTop:"2%",
                                    fontWeight:400,
                                    lineHeight:1.5
                                }}> Alamat :</Typography>
                                <Typography class="MuiTypography-root MuiTypography-body1" style={{
                                    color:"rgb(112, 117, 133)",
                                    fontSize:"10px",
                                    fontWeight:400,
                                    lineHeight:1.5,
                                    margin:0
                                }}></Typography>
                            </div>
                            </div>
                            <hr class="MuiDivider-root" style={{
                                border:"none",
                                height:"1px",
                                margin:0,
                                flexShrink:0,
                                backgroundColor:"rgba(0, 0, 0, 0.12)",
                                boxSizing:"inherit",
                                display:"block",
                                unicodeBidi:"isolate",
                                marginBlockStart:"0.5em",
                                marginBlockEnd:"0.5em",
                                marginInlineStart:"auto",
                                marginInlineEnd:"auto",
                            }}></hr>
                    </div>
                    </div>
                    <div class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-12" style={{
                        flexGrow:0,
                        maxWidth:"100%",
                        flexBasis:"100%",
                        margin:0,
                        boxSizing:"border-box",
                        display:"block",
                    }}>
                        <div class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-12" style={{
                        flexGrow:0,
                        maxWidth:"100%",
                        flexBasis:"100%",
                        margin:0,
                        boxSizing:"border-box",
                        display:"block",
                    }}>
                        <div style={{
                            padding:"12px 16px",
                            boxSizing:"inherit",
                            display:"block"
                        }}>
                            <div class="MuiGrid-root jss60 MuiGrid-item MuiGrid-grid-xs-12" style={{
                                display:"flex",
                                justifyContent:"Space-Between",
                                flexGrow:0,
                                flexBasis:"100%",
                                maxWidth:"100%",
                                margin:0,
                                boxSizing:"border-box"
                            }}>
                                <Typography class="MuiTypography-root MuiTypography-body1" style={{
                                    fontSize:"15px",
                                    fontWeight:400,
                                    lineHeight:1.5,
                                    margin:0,
                                    boxSizing:"inherit",
                                    display:"block",
                                    marginBlockStart:"1em",
                                    marginBlockEnd:"1em",
                                    marginInlineEnd:"0px",
                                    marginInlineStart:"0px",
                                    marginBottom:"5px"
                                }}><b>Cluster Sragen</b></Typography>
                                <img src="https://goodpangan.com//static/media/badge.dda219fb.svg">

                                </img>
                            </div>
                            <div class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-12" style={{
                                flexGrow:0,
                                maxWidth:"!00%",
                                flexBasis:"100%",
                                margin:0,
                                boxSizing:"border-box",
                                
                            }}>
                                <Typography class="MuiTypography-root jss62 MuiTypography-body1" style={{
                                    paddingTop:0,
                                    width:"100%",
                                    fontSize:"13px",
                                    fontWeight:400,
                                    lineHeight:1.5,
                                    margin:0,
                                    boxSizing:"inherit"
                                }}>Produk terbaik pasar ini:</Typography>
                                <Typography class="MuiTypography-root MuiTypography-body1" style={{
                                    color:"rgb(112, 117, 133)",
                                    fontSize:"10px",
                                    fontWeight:400,
                                    lineHeight:1.5,
                                    margin:0
                                }}></Typography>
                                <Typography class="MuiTypography-root MuiTypography-body1" style={{
                                    fontSize:"13px",
                                    paddingTop:"2%",
                                    fontWeight:400,
                                    lineHeight:1.5
                                }}> Alamat :</Typography>
                                <Typography class="MuiTypography-root MuiTypography-body1" style={{
                                    color:"rgb(112, 117, 133)",
                                    fontSize:"10px",
                                    fontWeight:400,
                                    lineHeight:1.5,
                                    margin:0
                                }}></Typography>
                            </div>
                            </div>
                            <hr class="MuiDivider-root" style={{
                                border:"none",
                                height:"1px",
                                margin:0,
                                flexShrink:0,
                                backgroundColor:"rgba(0, 0, 0, 0.12)",
                                boxSizing:"inherit",
                                display:"block",
                                unicodeBidi:"isolate",
                                marginBlockStart:"0.5em",
                                marginBlockEnd:"0.5em",
                                marginInlineStart:"auto",
                                marginInlineEnd:"auto",
                            }}></hr>
                    </div>
                    </div>
                </div>
                            </div>
                            </div>
                      
    
    )
}
export default Market;