import React from "react"
import ArrowBackIosRoundedIcon from "@material-ui/icons/ArrowBackIosRounded";
import IconButton from "@material-ui/core/IconButton";
import { useRouter} from "next/router";
import { Typography } from "@material-ui/core";
import ButtonTerlaris from "../Component/ButtonTerlaris";

function topseller() {
    const router = useRouter()
    return(
      
           <div>
               <main class="MuiContainer-root jss1536 MuiContainer-maxWidthXs" style={{
                   height:"100%",
                   padding:0,
                   minHeight:"100vh",
                   borderLeft:"1px solid #f1f1f1",
                   borderRight:"1px solid #f1f1f1",
                   paddingTop:"64px",
                   marginBottom:0,
                   backgroundColor:"#FAFAFA",
                   maxWidth:"444px",
                   width:"100%",
                   display:"block",
                   boxSizing:"border-box",
                   marginLeft:"auto",
                   marginRight:"auto"
               }}>
                   <div class="MuiBox-root jss79 jss70 jss59" style={{
                       top:0,
                       width:"100%",
                       color:"white",
                       zIndex:99,
                       position:"fixed",
                       maxWidth:"442px",
                       background:"white",
                       display:"flex",
                       justifyContent:"center"
                   }}>
                       <header class="MuiPaper-root MuiAppBar-root MuiAppBar-positionStatic MuiAppBar-colorPrimary jss69 jss58 MuiPaper-elevation0" style={{
                           backgroundColor:"white",
                           color:"black",
                           top:0,
                           width:"100%",
                           zIndex:999,
                           position:"fixed",
                           maxWidth:"442px",
                           boxShadow:"0px 1px 5px rgb(0 0 0 / 5%)",
                           maxHeight:"120px"
                       }}>
                           <div class="MuiToolbar-root MuiToolbar-dense MuiToolbar-gutters" style={{
                               minHeight:"64px",
                               paddingLeft:"16px",
                               paddingRight:"16px",
                               display:"flex",
                               position:"relative",
                               alignItems:"center"
                           }}>
                                <IconButton
              onClick={()=>router.push("/")}
              edge="start"
              style={{
                color: "#F4783B"
              }}
            >
              <ArrowBackIosRoundedIcon style={{
                   fontSize:"1.2em",
              }}/> 
            </IconButton>

            <h6 class="MuiTypography-root jss73 jss62 MuiTypography-subtitle1 MuiTypography-alignLeft" style={{
                flexGrow:1,
                textAlign:"left",
                fontSize:"1rem",
                fontWeight:400,
                lineHeight:1.75,
                margin:0,
                display:"block",
                boxSizing:"inherit"
            }}><strong style={{
                fontWeight:700
            }}>Produk Terlaris</strong></h6>
            <span class="MuiTypography-root jss74 jss63 MuiTypography-caption" style={{
                color:"rgb(255, 118, 50)",
                fontWeight:400,
                lineHeight:1.66,
                fontSize:"0.78rem",
                margin:0
            }}><strong style={{
                color:"rgb(255, 118, 50)",
                boxSizing:"inherit"
            }}>Pilih Produk</strong></span>
                           </div>
                       </header>
                   </div>
                  <div class="MuiPaper-root jss56 MuiPaper-elevation0 MuiPaper-rounded" style={{
                      borderRadius:0,
                      boxShadow:"none",
                      color:"rgba(0, 0, 0, 0.87)",
                      transition:"box-shadow 300ms cubic-bezier(0.4, 0, 0.2, 1) 0ms",
                    
                  }}>
                      <div class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-12" style={{
                          flexGrow:0,
                          maxWidth:"100%",
                          flexBasis:"100%",
                          margin:0,
                          boxSizing:"border-box",
                          display:"block"
                      }}>
                          <div class="MuiBox-root jss88 jss80" style={{
                              padding:"16px",
                              borderRadius:"8px",
                          }}>
                              <div class="MuiGrid-root MuiGrid-container" style={{
                                  width:"100%",
                                  display:"flex",
                                  flexWrap:"wrap",
                                  boxSizing:"border-box"
                              }}>
                                  <div class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-3" style={{
                                      flexGrow:0,
                                      maxWidth:"25%",
                                      flexBasis:"25%",
                                      margin:0,
                                      boxSizing:"border-box"
                                  }}>
                                      <div class="MuiCardMedia-root jss81" style={{
                                          backgroundImage:"https://dashboard.goodpangan.com/wp-content/uploads/2020/07/FB_IMG_1566134767733.jpg",
                                          height:"100%",
                                          borderRadius:"8px",
                                          display:"block",
                                          backgroundSize:"cover",
                                          backgroundRepeat:"no-repeat",
                                          backgroundPosition:"center",
                                          boxSizing:"inherit"
                                      }}>
                                          <div class="jss83" style={{
                                              height:"-webkit-fill-available",
                                              display:"flex",
                                              flexDirection:"column",
                                              justifyContent:"flex-end"
                                          }}></div>
                                      </div>
                                  </div>
                                  <div class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-6" style={{
                                      flexGrow:0,
                                      maxWidth:"50%",
                                      flexBasis:"50%",
                                      margin:0,
                                      boxSizing:"border-box"
                                  }}>
                                      <div class="MuiCardContent-root" style={{
                                          paddingBottom:"24px",
                                          padding:"16px",
                                          boxSizing:"inherit"
                                      }}>
                                          <Typography class="MuiTypography-root jss82 MuiTypography-body1" style={{
                                              marginBottom:"10px",
                                              color:"#000000",
                                              fontSize:"14px",
                                              fontWeight:400,
                                              lineHeight:1.5,
                                              margin:0
                                          }}>Ain Drop II</Typography>
                                          <Typography class="MuiTypography-root jss85 MuiTypography-body1" style={{
                                              paddingTop:"30px",
                                              color:"#F1885B",
                                              fontSize:"13px",
                                              fontWeight:"bold",
                                              lineHeight:1.5,
                                              margin:0
                                          }}>Rp 75.000
                                          <b style={{
                                              color:"rgb(199, 199, 201)",
                                              fontSize:"10px",
                                              fontWeight:"bold",
                                              boxSizing:"inherit"
                                          }}>
                                               <b style={{
                                              color:"rgb(199, 199, 201)",
                                              fontSize:"10px",
                                              fontWeight:"bold",
                                              boxSizing:"inherit"
                                          }}></b>
                                          </b>
                                          </Typography>
                                      </div>
                                  </div>
                                  <div class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-3" align="right" style={{
                                         flexGrow:0,
                                         maxWidth:"25%",
                                         flexBasis:"25%",
                                         margin:0,
                                         boxSizing:"border-box"
                                  }}>
                                      <div style={{
                                          minHeight:"80px",
                                          minWidth:"100%",
                                          boxSizing:"inherit",
                                          display:"block"
                                      }}>
                                  </div>
                                  <div>
                                      <div>
                                         <ButtonTerlaris/>
                                      </div>
                                  </div>
                                  </div>
                              </div>
                          </div>
                          <hr class="MuiDivider-root" style={{
                              border:"none",
                              height:"1px",
                              margin:0,
                              flexShrink:0,
                              backgroundColor:"rgba(0, 0, 0, 0.12)",
                              boxSizing:"inherit",
                              display:"block",
                              unicodeBidi:"isolate",
                              marginBlockStart:"0.5em",
                              marginBlockEnd:"0.5em",
                              marginInlineEnd:"auto",
                              marginInlineStart:"auto",
                              overflow:"hidden"
                          }}></hr>
                      </div>
                      <div class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-12" style={{
                          flexGrow:0,
                          maxWidth:"100%",
                          flexBasis:"100%",
                          margin:0,
                          boxSizing:"border-box",
                          display:"block"
                      }}>
                          <div class="MuiBox-root jss88 jss80" style={{
                              padding:"16px",
                              borderRadius:"8px",
                          }}>
                              <div class="MuiGrid-root MuiGrid-container" style={{
                                  width:"100%",
                                  display:"flex",
                                  flexWrap:"wrap",
                                  boxSizing:"border-box"
                              }}>
                                  <div class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-3" style={{
                                      flexGrow:0,
                                      maxWidth:"25%",
                                      flexBasis:"25%",
                                      margin:0,
                                      boxSizing:"border-box"
                                  }}>
                                      <div class="MuiCardMedia-root jss81" style={{
                                          backgroundImage:"https://dashboard.goodpangan.com/wp-content/uploads/2020/07/fix.am1_.png",
                                          height:"100%",
                                          borderRadius:"8px",
                                          display:"block",
                                          backgroundSize:"cover",
                                          backgroundRepeat:"no-repeat",
                                          backgroundPosition:"center",
                                          boxSizing:"inherit"
                                      }}>
                                          <div class="jss83" style={{
                                              height:"-webkit-fill-available",
                                              display:"flex",
                                              flexDirection:"column",
                                              justifyContent:"flex-end"
                                          }}></div>
                                      </div>
                                  </div>
                                  <div class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-6" style={{
                                      flexGrow:0,
                                      maxWidth:"50%",
                                      flexBasis:"50%",
                                      margin:0,
                                      boxSizing:"border-box"
                                  }}>
                                      <div class="MuiCardContent-root" style={{
                                          paddingBottom:"24px",
                                          padding:"16px",
                                          boxSizing:"inherit"
                                      }}>
                                          <Typography class="MuiTypography-root jss82 MuiTypography-body1" style={{
                                              marginBottom:"10px",
                                              color:"#000000",
                                              fontSize:"14px",
                                              fontWeight:400,
                                              lineHeight:1.5,
                                              margin:0
                                          }}>AM-I</Typography>
                                          <Typography class="MuiTypography-root jss85 MuiTypography-body1" style={{
                                              paddingTop:"30px",
                                              color:"#F1885B",
                                              fontSize:"13px",
                                              fontWeight:"bold",
                                              lineHeight:1.5,
                                              margin:0
                                          }}>Rp 350.000
                                          <b style={{
                                              color:"rgb(199, 199, 201)",
                                              fontSize:"10px",
                                              fontWeight:"bold",
                                              boxSizing:"inherit"
                                          }}>
                                               <b style={{
                                              color:"rgb(199, 199, 201)",
                                              fontSize:"10px",
                                              fontWeight:"bold",
                                              boxSizing:"inherit"
                                          }}></b>
                                          </b>
                                          </Typography>
                                      </div>
                                  </div>
                                  <div class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-3" align="right" style={{
                                         flexGrow:0,
                                         maxWidth:"25%",
                                         flexBasis:"25%",
                                         margin:0,
                                         boxSizing:"border-box"
                                  }}>
                                      <div style={{
                                          minHeight:"80px",
                                          minWidth:"100%",
                                          boxSizing:"inherit",
                                          display:"block"
                                      }}>
                                  </div>
                                  <div>
                                      <div>
                                         <ButtonTerlaris/>
                                      </div>
                                  </div>
                                  </div>
                              </div>
                          </div>
                          <hr class="MuiDivider-root" style={{
                              border:"none",
                              height:"1px",
                              margin:0,
                              flexShrink:0,
                              backgroundColor:"rgba(0, 0, 0, 0.12)",
                              boxSizing:"inherit",
                              display:"block",
                              unicodeBidi:"isolate",
                              marginBlockStart:"0.5em",
                              marginBlockEnd:"0.5em",
                              marginInlineEnd:"auto",
                              marginInlineStart:"auto",
                              overflow:"hidden"
                          }}></hr>
                      </div>
                      <div class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-12" style={{
                          flexGrow:0,
                          maxWidth:"100%",
                          flexBasis:"100%",
                          margin:0,
                          boxSizing:"border-box",
                          display:"block"
                      }}>
                          <div class="MuiBox-root jss88 jss80" style={{
                              padding:"16px",
                              borderRadius:"8px",
                          }}>
                              <div class="MuiGrid-root MuiGrid-container" style={{
                                  width:"100%",
                                  display:"flex",
                                  flexWrap:"wrap",
                                  boxSizing:"border-box"
                              }}>
                                  <div class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-3" style={{
                                      flexGrow:0,
                                      maxWidth:"25%",
                                      flexBasis:"25%",
                                      margin:0,
                                      boxSizing:"border-box"
                                  }}>
                                      <div class="MuiCardMedia-root jss81" style={{
                                          backgroundImage:"https://dashboard.goodpangan.com/wp-content/uploads/2020/07/fix.baiduri.png",
                                          height:"100%",
                                          borderRadius:"8px",
                                          display:"block",
                                          backgroundSize:"cover",
                                          backgroundRepeat:"no-repeat",
                                          backgroundPosition:"center",
                                          boxSizing:"inherit"
                                      }}>
                                          <div class="jss83" style={{
                                              height:"-webkit-fill-available",
                                              display:"flex",
                                              flexDirection:"column",
                                              justifyContent:"flex-end"
                                          }}></div>
                                      </div>
                                  </div>
                                  <div class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-6" style={{
                                      flexGrow:0,
                                      maxWidth:"50%",
                                      flexBasis:"50%",
                                      margin:0,
                                      boxSizing:"border-box"
                                  }}>
                                      <div class="MuiCardContent-root" style={{
                                          paddingBottom:"24px",
                                          padding:"16px",
                                          boxSizing:"inherit"
                                      }}>
                                          <Typography class="MuiTypography-root jss82 MuiTypography-body1" style={{
                                              marginBottom:"10px",
                                              color:"#000000",
                                              fontSize:"14px",
                                              fontWeight:400,
                                              lineHeight:1.5,
                                              margin:0
                                          }}>Baiduri Pak Haji</Typography>
                                          <Typography class="MuiTypography-root jss85 MuiTypography-body1" style={{
                                              paddingTop:"30px",
                                              color:"#F1885B",
                                              fontSize:"13px",
                                              fontWeight:"bold",
                                              lineHeight:1.5,
                                              margin:0
                                          }}>Rp 130.000
                                          <b style={{
                                              color:"rgb(199, 199, 201)",
                                              fontSize:"10px",
                                              fontWeight:"bold",
                                              boxSizing:"inherit"
                                          }}>
                                               <b style={{
                                              color:"rgb(199, 199, 201)",
                                              fontSize:"10px",
                                              fontWeight:"bold",
                                              boxSizing:"inherit"
                                          }}></b>
                                          </b>
                                          </Typography>
                                      </div>
                                  </div>
                                  <div class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-3" align="right" style={{
                                         flexGrow:0,
                                         maxWidth:"25%",
                                         flexBasis:"25%",
                                         margin:0,
                                         boxSizing:"border-box"
                                  }}>
                                      <div style={{
                                          minHeight:"80px",
                                          minWidth:"100%",
                                          boxSizing:"inherit",
                                          display:"block"
                                      }}>
                                  </div>
                                  <div>
                                      <div>
                                        <ButtonTerlaris/>
                                      </div>
                                  </div>
                                  </div>
                              </div>
                          </div>
                          <hr class="MuiDivider-root" style={{
                              border:"none",
                              height:"1px",
                              margin:0,
                              flexShrink:0,
                              backgroundColor:"rgba(0, 0, 0, 0.12)",
                              boxSizing:"inherit",
                              display:"block",
                              unicodeBidi:"isolate",
                              marginBlockStart:"0.5em",
                              marginBlockEnd:"0.5em",
                              marginInlineEnd:"auto",
                              marginInlineStart:"auto",
                              overflow:"hidden"
                          }}></hr>
                      </div>
                      <div class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-12" style={{
                          flexGrow:0,
                          maxWidth:"100%",
                          flexBasis:"100%",
                          margin:0,
                          boxSizing:"border-box",
                          display:"block"
                      }}>
                          <div class="MuiBox-root jss88 jss80" style={{
                              padding:"16px",
                              borderRadius:"8px",
                          }}>
                              <div class="MuiGrid-root MuiGrid-container" style={{
                                  width:"100%",
                                  display:"flex",
                                  flexWrap:"wrap",
                                  boxSizing:"border-box"
                              }}>
                                  <div class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-3" style={{
                                      flexGrow:0,
                                      maxWidth:"25%",
                                      flexBasis:"25%",
                                      margin:0,
                                      boxSizing:"border-box"
                                  }}>
                                      <div class="MuiCardMedia-root jss81" style={{
                                          backgroundImage:"https://dashboard.goodpangan.com/wp-content/uploads/2020/07/5ad9b2e8f47c95e6c32f03bb4a9e67f6.jpg",
                                          height:"100%",
                                          borderRadius:"8px",
                                          display:"block",
                                          backgroundSize:"cover",
                                          backgroundRepeat:"no-repeat",
                                          backgroundPosition:"center",
                                          boxSizing:"inherit"
                                      }}>
                                          <div class="jss83" style={{
                                              height:"-webkit-fill-available",
                                              display:"flex",
                                              flexDirection:"column",
                                              justifyContent:"flex-end"
                                          }}></div>
                                      </div>
                                  </div>
                                  <div class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-6" style={{
                                      flexGrow:0,
                                      maxWidth:"50%",
                                      flexBasis:"50%",
                                      margin:0,
                                      boxSizing:"border-box"
                                  }}>
                                      <div class="MuiCardContent-root" style={{
                                          paddingBottom:"24px",
                                          padding:"16px",
                                          boxSizing:"inherit"
                                      }}>
                                          <Typography class="MuiTypography-root jss82 MuiTypography-body1" style={{
                                              marginBottom:"10px",
                                              color:"#000000",
                                              fontSize:"14px",
                                              fontWeight:400,
                                              lineHeight:1.5,
                                              margin:0
                                          }}>Bedak Nur</Typography>
                                          <Typography class="MuiTypography-root jss85 MuiTypography-body1" style={{
                                              paddingTop:"30px",
                                              color:"#F1885B",
                                              fontSize:"13px",
                                              fontWeight:"bold",
                                              lineHeight:1.5,
                                              margin:0
                                          }}>Rp 105.000
                                          <b style={{
                                              color:"rgb(199, 199, 201)",
                                              fontSize:"10px",
                                              fontWeight:"bold",
                                              boxSizing:"inherit"
                                          }}>
                                               <b style={{
                                              color:"rgb(199, 199, 201)",
                                              fontSize:"10px",
                                              fontWeight:"bold",
                                              boxSizing:"inherit"
                                          }}></b>
                                          </b>
                                          </Typography>
                                      </div>
                                  </div>
                                  <div class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-3" align="right" style={{
                                         flexGrow:0,
                                         maxWidth:"25%",
                                         flexBasis:"25%",
                                         margin:0,
                                         boxSizing:"border-box"
                                  }}>
                                      <div style={{
                                          minHeight:"80px",
                                          minWidth:"100%",
                                          boxSizing:"inherit",
                                          display:"block"
                                      }}>
                                  </div>
                                  <div>
                                      <div>
                                          <ButtonTerlaris/>
                                      </div>
                                  </div>
                                  </div>
                              </div>
                          </div>
                          <hr class="MuiDivider-root" style={{
                              border:"none",
                              height:"1px",
                              margin:0,
                              flexShrink:0,
                              backgroundColor:"rgba(0, 0, 0, 0.12)",
                              boxSizing:"inherit",
                              display:"block",
                              unicodeBidi:"isolate",
                              marginBlockStart:"0.5em",
                              marginBlockEnd:"0.5em",
                              marginInlineEnd:"auto",
                              marginInlineStart:"auto",
                              overflow:"hidden"
                          }}></hr>
                      </div>
                      <div class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-12" style={{
                          flexGrow:0,
                          maxWidth:"100%",
                          flexBasis:"100%",
                          margin:0,
                          boxSizing:"border-box",
                          display:"block"
                      }}>
                          <div class="MuiBox-root jss88 jss80" style={{
                              padding:"16px",
                              borderRadius:"8px",
                          }}>
                              <div class="MuiGrid-root MuiGrid-container" style={{
                                  width:"100%",
                                  display:"flex",
                                  flexWrap:"wrap",
                                  boxSizing:"border-box"
                              }}>
                                  <div class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-3" style={{
                                      flexGrow:0,
                                      maxWidth:"25%",
                                      flexBasis:"25%",
                                      margin:0,
                                      boxSizing:"border-box"
                                  }}>
                                      <div class="MuiCardMedia-root jss81" style={{
                                          backgroundImage:"https://dashboard.goodpangan.com/wp-content/uploads/2020/07/Bedak-Talc.png",
                                          height:"100%",
                                          borderRadius:"8px",
                                          display:"block",
                                          backgroundSize:"cover",
                                          backgroundRepeat:"no-repeat",
                                          backgroundPosition:"center",
                                          boxSizing:"inherit"
                                      }}>
                                          <div class="jss83" style={{
                                              height:"-webkit-fill-available",
                                              display:"flex",
                                              flexDirection:"column",
                                              justifyContent:"flex-end"
                                          }}></div>
                                      </div>
                                  </div>
                                  <div class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-6" style={{
                                      flexGrow:0,
                                      maxWidth:"50%",
                                      flexBasis:"50%",
                                      margin:0,
                                      boxSizing:"border-box"
                                  }}>
                                      <div class="MuiCardContent-root" style={{
                                          paddingBottom:"24px",
                                          padding:"16px",
                                          boxSizing:"inherit"
                                      }}>
                                          <Typography class="MuiTypography-root jss82 MuiTypography-body1" style={{
                                              marginBottom:"10px",
                                              color:"#000000",
                                              fontSize:"14px",
                                              fontWeight:400,
                                              lineHeight:1.5,
                                              margin:0
                                          }}>Bedak Talcum Family</Typography>
                                          <Typography class="MuiTypography-root jss85 MuiTypography-body1" style={{
                                              paddingTop:"30px",
                                              color:"#F1885B",
                                              fontSize:"13px",
                                              fontWeight:"bold",
                                              lineHeight:1.5,
                                              margin:0
                                          }}>Rp 22.000
                                          <b style={{
                                              color:"rgb(199, 199, 201)",
                                              fontSize:"10px",
                                              fontWeight:"bold",
                                              boxSizing:"inherit"
                                          }}>
                                               <b style={{
                                              color:"rgb(199, 199, 201)",
                                              fontSize:"10px",
                                              fontWeight:"bold",
                                              boxSizing:"inherit"
                                          }}></b>
                                          </b>
                                          </Typography>
                                      </div>
                                  </div>
                                  <div class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-3" align="right" style={{
                                         flexGrow:0,
                                         maxWidth:"25%",
                                         flexBasis:"25%",
                                         margin:0,
                                         boxSizing:"border-box"
                                  }}>
                                      <div style={{
                                          minHeight:"80px",
                                          minWidth:"100%",
                                          boxSizing:"inherit",
                                          display:"block"
                                      }}>
                                  </div>
                                  <div>
                                      <div>
                                         <ButtonTerlaris/>
                                      </div>
                                  </div>
                                  </div>
                              </div>
                          </div>
                          <hr class="MuiDivider-root" style={{
                              border:"none",
                              height:"1px",
                              margin:0,
                              flexShrink:0,
                              backgroundColor:"rgba(0, 0, 0, 0.12)",
                              boxSizing:"inherit",
                              display:"block",
                              unicodeBidi:"isolate",
                              marginBlockStart:"0.5em",
                              marginBlockEnd:"0.5em",
                              marginInlineEnd:"auto",
                              marginInlineStart:"auto",
                              overflow:"hidden"
                          }}></hr>
                      </div>
                      <div class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-12" style={{
                          flexGrow:0,
                          maxWidth:"100%",
                          flexBasis:"100%",
                          margin:0,
                          boxSizing:"border-box",
                          display:"block"
                      }}>
                          <div class="MuiBox-root jss88 jss80" style={{
                              padding:"16px",
                              borderRadius:"8px",
                          }}>
                              <div class="MuiGrid-root MuiGrid-container" style={{
                                  width:"100%",
                                  display:"flex",
                                  flexWrap:"wrap",
                                  boxSizing:"border-box"
                              }}>
                                  <div class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-3" style={{
                                      flexGrow:0,
                                      maxWidth:"25%",
                                      flexBasis:"25%",
                                      margin:0,
                                      boxSizing:"border-box"
                                  }}>
                                      <div class="MuiCardMedia-root jss81" style={{
                                          backgroundImage:"https://dashboard.goodpangan.com/wp-content/uploads/2020/07/a295cc9768605a53a73466065a803a99-removebg-preview.png",
                                          height:"100%",
                                          borderRadius:"8px",
                                          display:"block",
                                          backgroundSize:"cover",
                                          backgroundRepeat:"no-repeat",
                                          backgroundPosition:"center",
                                          boxSizing:"inherit"
                                      }}>
                                          <div class="jss83" style={{
                                              height:"-webkit-fill-available",
                                              display:"flex",
                                              flexDirection:"column",
                                              justifyContent:"flex-end"
                                          }}></div>
                                      </div>
                                  </div>
                                  <div class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-6" style={{
                                      flexGrow:0,
                                      maxWidth:"50%",
                                      flexBasis:"50%",
                                      margin:0,
                                      boxSizing:"border-box"
                                  }}>
                                      <div class="MuiCardContent-root" style={{
                                          paddingBottom:"24px",
                                          padding:"16px",
                                          boxSizing:"inherit"
                                      }}>
                                          <Typography class="MuiTypography-root jss82 MuiTypography-body1" style={{
                                              marginBottom:"10px",
                                              color:"#000000",
                                              fontSize:"14px",
                                              fontWeight:400,
                                              lineHeight:1.5,
                                              margin:0
                                          }}>Black Salt</Typography>
                                          <Typography class="MuiTypography-root jss85 MuiTypography-body1" style={{
                                              paddingTop:"30px",
                                              color:"#F1885B",
                                              fontSize:"13px",
                                              fontWeight:"bold",
                                              lineHeight:1.5,
                                              margin:0
                                          }}>Rp 60.000
                                          <b style={{
                                              color:"rgb(199, 199, 201)",
                                              fontSize:"10px",
                                              fontWeight:"bold",
                                              boxSizing:"inherit"
                                          }}>
                                               <b style={{
                                              color:"rgb(199, 199, 201)",
                                              fontSize:"10px",
                                              fontWeight:"bold",
                                              boxSizing:"inherit"
                                          }}></b>
                                          </b>
                                          </Typography>
                                      </div>
                                  </div>
                                  <div class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-3" align="right" style={{
                                         flexGrow:0,
                                         maxWidth:"25%",
                                         flexBasis:"25%",
                                         margin:0,
                                         boxSizing:"border-box"
                                  }}>
                                      <div style={{
                                          minHeight:"80px",
                                          minWidth:"100%",
                                          boxSizing:"inherit",
                                          display:"block"
                                      }}>
                                  </div>
                                  <div>
                                      <div>
                                          <ButtonTerlaris/>
                                      </div>
                                  </div>
                                  </div>
                              </div>
                          </div>
                          <hr class="MuiDivider-root" style={{
                              border:"none",
                              height:"1px",
                              margin:0,
                              flexShrink:0,
                              backgroundColor:"rgba(0, 0, 0, 0.12)",
                              boxSizing:"inherit",
                              display:"block",
                              unicodeBidi:"isolate",
                              marginBlockStart:"0.5em",
                              marginBlockEnd:"0.5em",
                              marginInlineEnd:"auto",
                              marginInlineStart:"auto",
                              overflow:"hidden"
                          }}></hr>
                      </div>
                      <div class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-12" style={{
                          flexGrow:0,
                          maxWidth:"100%",
                          flexBasis:"100%",
                          margin:0,
                          boxSizing:"border-box",
                          display:"block"
                      }}>
                          <div class="MuiBox-root jss88 jss80" style={{
                              padding:"16px",
                              borderRadius:"8px",
                          }}>
                              <div class="MuiGrid-root MuiGrid-container" style={{
                                  width:"100%",
                                  display:"flex",
                                  flexWrap:"wrap",
                                  boxSizing:"border-box"
                              }}>
                                  <div class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-3" style={{
                                      flexGrow:0,
                                      maxWidth:"25%",
                                      flexBasis:"25%",
                                      margin:0,
                                      boxSizing:"border-box"
                                  }}>
                                      <div class="MuiCardMedia-root jss81" style={{
                                          backgroundImage:"https://dashboard.goodpangan.com/wp-content/uploads/2020/07/108635dc0740571b985bef71a20dcbbb-removebg-preview.png",
                                          height:"100%",
                                          borderRadius:"8px",
                                          display:"block",
                                          backgroundSize:"cover",
                                          backgroundRepeat:"no-repeat",
                                          backgroundPosition:"center",
                                          boxSizing:"inherit"
                                      }}>
                                          <div class="jss83" style={{
                                              height:"-webkit-fill-available",
                                              display:"flex",
                                              flexDirection:"column",
                                              justifyContent:"flex-end"
                                          }}></div>
                                      </div>
                                  </div>
                                  <div class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-6" style={{
                                      flexGrow:0,
                                      maxWidth:"50%",
                                      flexBasis:"50%",
                                      margin:0,
                                      boxSizing:"border-box"
                                  }}>
                                      <div class="MuiCardContent-root" style={{
                                          paddingBottom:"24px",
                                          padding:"16px",
                                          boxSizing:"inherit"
                                      }}>
                                          <Typography class="MuiTypography-root jss82 MuiTypography-body1" style={{
                                              marginBottom:"10px",
                                              color:"#000000",
                                              fontSize:"14px",
                                              fontWeight:400,
                                              lineHeight:1.5,
                                              margin:0
                                          }}>BTS Spray</Typography>
                                          <Typography class="MuiTypography-root jss85 MuiTypography-body1" style={{
                                              paddingTop:"30px",
                                              color:"#F1885B",
                                              fontSize:"13px",
                                              fontWeight:"bold",
                                              lineHeight:1.5,
                                              margin:0
                                          }}>Rp 300.000
                                          <b style={{
                                              color:"rgb(199, 199, 201)",
                                              fontSize:"10px",
                                              fontWeight:"bold",
                                              boxSizing:"inherit"
                                          }}>
                                               <b style={{
                                              color:"rgb(199, 199, 201)",
                                              fontSize:"10px",
                                              fontWeight:"bold",
                                              boxSizing:"inherit"
                                          }}></b>
                                          </b>
                                          </Typography>
                                      </div>
                                  </div>
                                  <div class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-3" align="right" style={{
                                         flexGrow:0,
                                         maxWidth:"25%",
                                         flexBasis:"25%",
                                         margin:0,
                                         boxSizing:"border-box"
                                  }}>
                                      <div style={{
                                          minHeight:"80px",
                                          minWidth:"100%",
                                          boxSizing:"inherit",
                                          display:"block"
                                      }}>
                                  </div>
                                  <div>
                                      <div>
                                          <ButtonTerlaris/>
                                      </div>
                                  </div>
                                  </div>
                              </div>
                          </div>
                          <hr class="MuiDivider-root" style={{
                              border:"none",
                              height:"1px",
                              margin:0,
                              flexShrink:0,
                              backgroundColor:"rgba(0, 0, 0, 0.12)",
                              boxSizing:"inherit",
                              display:"block",
                              unicodeBidi:"isolate",
                              marginBlockStart:"0.5em",
                              marginBlockEnd:"0.5em",
                              marginInlineEnd:"auto",
                              marginInlineStart:"auto",
                              overflow:"hidden"
                          }}></hr>
                      </div>
                      <div class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-12" style={{
                          flexGrow:0,
                          maxWidth:"100%",
                          flexBasis:"100%",
                          margin:0,
                          boxSizing:"border-box",
                          display:"block"
                      }}>
                          <div class="MuiBox-root jss88 jss80" style={{
                              padding:"16px",
                              borderRadius:"8px",
                          }}>
                              <div class="MuiGrid-root MuiGrid-container" style={{
                                  width:"100%",
                                  display:"flex",
                                  flexWrap:"wrap",
                                  boxSizing:"border-box"
                              }}>
                                  <div class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-3" style={{
                                      flexGrow:0,
                                      maxWidth:"25%",
                                      flexBasis:"25%",
                                      margin:0,
                                      boxSizing:"border-box"
                                  }}>
                                      <div class="MuiCardMedia-root jss81" style={{
                                          backgroundImage:"https://dashboard.goodpangan.com/wp-content/uploads/2020/07/94c827ee9c3ddd270b43267219e2c04c.jpg",
                                          height:"100%",
                                          borderRadius:"8px",
                                          display:"block",
                                          backgroundSize:"cover",
                                          backgroundRepeat:"no-repeat",
                                          backgroundPosition:"center",
                                          boxSizing:"inherit"
                                      }}>
                                          <div class="jss83" style={{
                                              height:"-webkit-fill-available",
                                              display:"flex",
                                              flexDirection:"column",
                                              justifyContent:"flex-end"
                                          }}></div>
                                      </div>
                                  </div>
                                  <div class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-6" style={{
                                      flexGrow:0,
                                      maxWidth:"50%",
                                      flexBasis:"50%",
                                      margin:0,
                                      boxSizing:"border-box"
                                  }}>
                                      <div class="MuiCardContent-root" style={{
                                          paddingBottom:"24px",
                                          padding:"16px",
                                          boxSizing:"inherit"
                                      }}>
                                          <Typography class="MuiTypography-root jss82 MuiTypography-body1" style={{
                                              marginBottom:"10px",
                                              color:"#000000",
                                              fontSize:"14px",
                                              fontWeight:400,
                                              lineHeight:1.5,
                                              margin:0
                                          }}>Chendra Beauty</Typography>
                                          <Typography class="MuiTypography-root jss85 MuiTypography-body1" style={{
                                              paddingTop:"30px",
                                              color:"#F1885B",
                                              fontSize:"13px",
                                              fontWeight:"bold",
                                              lineHeight:1.5,
                                              margin:0
                                          }}>Rp 450.000
                                          <b style={{
                                              color:"rgb(199, 199, 201)",
                                              fontSize:"10px",
                                              fontWeight:"bold",
                                              boxSizing:"inherit"
                                          }}>
                                               <b style={{
                                              color:"rgb(199, 199, 201)",
                                              fontSize:"10px",
                                              fontWeight:"bold",
                                              boxSizing:"inherit"
                                          }}></b>
                                          </b>
                                          </Typography>
                                      </div>
                                  </div>
                                  <div class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-3" align="right" style={{
                                         flexGrow:0,
                                         maxWidth:"25%",
                                         flexBasis:"25%",
                                         margin:0,
                                         boxSizing:"border-box"
                                  }}>
                                      <div style={{
                                          minHeight:"80px",
                                          minWidth:"100%",
                                          boxSizing:"inherit",
                                          display:"block"
                                      }}>
                                  </div>
                                  <div>
                                      <div>
                                          <ButtonTerlaris/>
                                      </div>
                                  </div>
                                  </div>
                              </div>
                          </div>
                          <hr class="MuiDivider-root" style={{
                              border:"none",
                              height:"1px",
                              margin:0,
                              flexShrink:0,
                              backgroundColor:"rgba(0, 0, 0, 0.12)",
                              boxSizing:"inherit",
                              display:"block",
                              unicodeBidi:"isolate",
                              marginBlockStart:"0.5em",
                              marginBlockEnd:"0.5em",
                              marginInlineEnd:"auto",
                              marginInlineStart:"auto",
                              overflow:"hidden"
                          }}></hr>
                      </div>
                      <div class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-12" style={{
                          flexGrow:0,
                          maxWidth:"100%",
                          flexBasis:"100%",
                          margin:0,
                          boxSizing:"border-box",
                          display:"block"
                      }}>
                          <div class="MuiBox-root jss88 jss80" style={{
                              padding:"16px",
                              borderRadius:"8px",
                          }}>
                              <div class="MuiGrid-root MuiGrid-container" style={{
                                  width:"100%",
                                  display:"flex",
                                  flexWrap:"wrap",
                                  boxSizing:"border-box"
                              }}>
                                  <div class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-3" style={{
                                      flexGrow:0,
                                      maxWidth:"25%",
                                      flexBasis:"25%",
                                      margin:0,
                                      boxSizing:"border-box"
                                  }}>
                                      <div class="MuiCardMedia-root jss81" style={{
                                          backgroundImage:"https://dashboard.goodpangan.com/wp-content/uploads/2020/07/fix.choco-soap.png",
                                          height:"100%",
                                          borderRadius:"8px",
                                          display:"block",
                                          backgroundSize:"cover",
                                          backgroundRepeat:"no-repeat",
                                          backgroundPosition:"center",
                                          boxSizing:"inherit"
                                      }}>
                                          <div class="jss83" style={{
                                              height:"-webkit-fill-available",
                                              display:"flex",
                                              flexDirection:"column",
                                              justifyContent:"flex-end"
                                          }}></div>
                                      </div>
                                  </div>
                                  <div class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-6" style={{
                                      flexGrow:0,
                                      maxWidth:"50%",
                                      flexBasis:"50%",
                                      margin:0,
                                      boxSizing:"border-box"
                                  }}>
                                      <div class="MuiCardContent-root" style={{
                                          paddingBottom:"24px",
                                          padding:"16px",
                                          boxSizing:"inherit"
                                      }}>
                                          <Typography class="MuiTypography-root jss82 MuiTypography-body1" style={{
                                              marginBottom:"10px",
                                              color:"#000000",
                                              fontSize:"14px",
                                              fontWeight:400,
                                              lineHeight:1.5,
                                              margin:0
                                          }}>Coco Soap</Typography>
                                          <Typography class="MuiTypography-root jss85 MuiTypography-body1" style={{
                                              paddingTop:"30px",
                                              color:"#F1885B",
                                              fontSize:"13px",
                                              fontWeight:"bold",
                                              lineHeight:1.5,
                                              margin:0
                                          }}>Rp 20.000
                                          <b style={{
                                              color:"rgb(199, 199, 201)",
                                              fontSize:"10px",
                                              fontWeight:"bold",
                                              boxSizing:"inherit"
                                          }}>
                                               <b style={{
                                              color:"rgb(199, 199, 201)",
                                              fontSize:"10px",
                                              fontWeight:"bold",
                                              boxSizing:"inherit"
                                          }}></b>
                                          </b>
                                          </Typography>
                                      </div>
                                  </div>
                                  <div class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-3" align="right" style={{
                                         flexGrow:0,
                                         maxWidth:"25%",
                                         flexBasis:"25%",
                                         margin:0,
                                         boxSizing:"border-box"
                                  }}>
                                      <div style={{
                                          minHeight:"80px",
                                          minWidth:"100%",
                                          boxSizing:"inherit",
                                          display:"block"
                                      }}>
                                  </div>
                                  <div>
                                      <div>
                                          <ButtonTerlaris/>
                                      </div>
                                  </div>
                                  </div>
                              </div>
                          </div>
                          <hr class="MuiDivider-root" style={{
                              border:"none",
                              height:"1px",
                              margin:0,
                              flexShrink:0,
                              backgroundColor:"rgba(0, 0, 0, 0.12)",
                              boxSizing:"inherit",
                              display:"block",
                              unicodeBidi:"isolate",
                              marginBlockStart:"0.5em",
                              marginBlockEnd:"0.5em",
                              marginInlineEnd:"auto",
                              marginInlineStart:"auto",
                              overflow:"hidden"
                          }}></hr>
                      </div>
                      <div class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-12" style={{
                          flexGrow:0,
                          maxWidth:"100%",
                          flexBasis:"100%",
                          margin:0,
                          boxSizing:"border-box",
                          display:"block"
                      }}>
                          <div class="MuiBox-root jss88 jss80" style={{
                              padding:"16px",
                              borderRadius:"8px",
                          }}>
                              <div class="MuiGrid-root MuiGrid-container" style={{
                                  width:"100%",
                                  display:"flex",
                                  flexWrap:"wrap",
                                  boxSizing:"border-box"
                              }}>
                                  <div class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-3" style={{
                                      flexGrow:0,
                                      maxWidth:"25%",
                                      flexBasis:"25%",
                                      margin:0,
                                      boxSizing:"border-box"
                                  }}>
                                      <div class="MuiCardMedia-root jss81" style={{
                                          backgroundImage:"https://dashboard.goodpangan.com/wp-content/uploads/2020/07/Screenshot_13.png",
                                          height:"100%",
                                          borderRadius:"8px",
                                          display:"block",
                                          backgroundSize:"cover",
                                          backgroundRepeat:"no-repeat",
                                          backgroundPosition:"center",
                                          boxSizing:"inherit"
                                      }}>
                                          <div class="jss83" style={{
                                              height:"-webkit-fill-available",
                                              display:"flex",
                                              flexDirection:"column",
                                              justifyContent:"flex-end"
                                          }}></div>
                                      </div>
                                  </div>
                                  <div class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-6" style={{
                                      flexGrow:0,
                                      maxWidth:"50%",
                                      flexBasis:"50%",
                                      margin:0,
                                      boxSizing:"border-box"
                                  }}>
                                      <div class="MuiCardContent-root" style={{
                                          paddingBottom:"24px",
                                          padding:"16px",
                                          boxSizing:"inherit"
                                      }}>
                                          <Typography class="MuiTypography-root jss82 MuiTypography-body1" style={{
                                              marginBottom:"10px",
                                              color:"#000000",
                                              fontSize:"14px",
                                              fontWeight:400,
                                              lineHeight:1.5,
                                              margin:0
                                          }}>Coffe Soap</Typography>
                                          <Typography class="MuiTypography-root jss85 MuiTypography-body1" style={{
                                              paddingTop:"30px",
                                              color:"#F1885B",
                                              fontSize:"13px",
                                              fontWeight:"bold",
                                              lineHeight:1.5,
                                              margin:0
                                          }}>Rp 20.000
                                          <b style={{
                                              color:"rgb(199, 199, 201)",
                                              fontSize:"10px",
                                              fontWeight:"bold",
                                              boxSizing:"inherit"
                                          }}>
                                               <b style={{
                                              color:"rgb(199, 199, 201)",
                                              fontSize:"10px",
                                              fontWeight:"bold",
                                              boxSizing:"inherit"
                                          }}></b>
                                          </b>
                                          </Typography>
                                      </div>
                                  </div>
                                  <div class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-3" align="right" style={{
                                         flexGrow:0,
                                         maxWidth:"25%",
                                         flexBasis:"25%",
                                         margin:0,
                                         boxSizing:"border-box"
                                  }}>
                                      <div style={{
                                          minHeight:"80px",
                                          minWidth:"100%",
                                          boxSizing:"inherit",
                                          display:"block"
                                      }}>
                                  </div>
                                  <div>
                                      <div>
                                          <ButtonTerlaris/>
                                      </div>
                                  </div>
                                  </div>
                              </div>
                          </div>
                          <hr class="MuiDivider-root" style={{
                              border:"none",
                              height:"1px",
                              margin:0,
                              flexShrink:0,
                              backgroundColor:"rgba(0, 0, 0, 0.12)",
                              boxSizing:"inherit",
                              display:"block",
                              unicodeBidi:"isolate",
                              marginBlockStart:"0.5em",
                              marginBlockEnd:"0.5em",
                              marginInlineEnd:"auto",
                              marginInlineStart:"auto",
                              overflow:"hidden"
                          }}></hr>
                      </div>
                  </div>
                  </main>
                  </div>
     
    )


}
export default topseller