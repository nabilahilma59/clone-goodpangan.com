import React from "react"
import IconButton from "@material-ui/core/IconButton";
import { useRouter} from "next/router";
import ArrowBackIosRoundedIcon from "@material-ui/icons/ArrowBackIosRounded";
import Tabs from "@material-ui/core/Tabs"
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Tab from "@material-ui/core/Tab"
import AppBar from "@material-ui/core/AppBar"
import Box from "@material-ui/core/Box"
import Typography from "@material-ui/core/Typography"

function TabPanel(props) {
    const { children, value, index, ...other } = props;
    
  
    return (
      <div
        role="tabpanel"
        hidden={value !== index}
        id={`scrollable-force-tabpanel-${index}`}
        aria-labelledby={`scrollable-force-tab-${index}`}
        {...other}
      >
        {value === index && (
          <Box p={3}>
            <Typography>{children}</Typography>
          </Box>
        )}
      </div>
    );
  }
  
  TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
  };
  
  function a11yProps(index) {
    return {
        
      id: `scrollable-force-tab-${index}`,
      'aria-controls': `scrollable-force-tabpanel-${index}`,
    };
  }
  
  const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
      width: '100%',
      backgroundColor: theme.palette.background.paper,
    },
  }));
  
 

function Category33() {
    const classes = useStyles();
    const [value, setValue] = React.useState(0);
  
    const handleChange = (event, newValue) => {
      setValue(newValue);
    };
    const router = useRouter()
    
    return(
        <div>
            <main class="MuiContainer-root jss96 MuiContainer-maxWidthXs" style={{
                height:"100%",
                padding:0,
                minHeight:"100vh",
                borderLeft:"1px solid #f1f1f1",
                borderRight:"1px solid #f1f1f1",
                backgroundColor:"#FAFAFA",
                maxWidth:"444px",
                width:"100%",
                display:"block",
                boxSizing:"border-box",
                marginLeft:"auto",
                marginRight:"auto"
            }}>
                <div class="MuiGrid-root jss108 MuiGrid-item MuiGrid-grid-xs-12" style={{
                    width:"100%",
                    zIndex:99,
                    position:"fixed",
                    maxWidth:"442px",
                    boxShadow:"0px 2px 8px rgb(0 0 0 / 12%)",
                    minHeight:"112px",
                    marginBottom:"10px",
                    flexGrow:0,
                    flexBasis:"100%",
                    margin:0,
                    boxSizing:"border-box"
                
                }}>
                    <div class="MuiBox-root jss135 jss126 jss115" style={{
                        top:0,
                        color:"white",
                        width:"100%",
                        zIndex:99,
                        position:"fixed",
                        maxWidth:"442px",
                        background:"white",
                        display:"flex",
                        justifyContent:"center"
                    }}>
                        <header class="MuiPaper-root MuiAppBar-root MuiAppBar-positionStatic MuiAppBar-colorPrimary jss125 jss114 MuiPaper-elevation0" style={{
                            backgroundColor:"white",
                            color:"black",
                            top:0,
                            width:"100%",
                            zIndex:999,
                            position:"fixed",
                            maxWidth:"442px",
                            boxShadow:"0px 1px 5px rgb(0 0 0 / 5%)",
                            maxHeight:"120px",
                        }}>
                            <div class="MuiToolbar-root MuiToolbar-dense MuiToolbar-gutters" style={{
                                minHeight:"64px",
                                paddingLeft:"16px",
                                paddingRight:"16px",
                                display:"flex",
                                position:"relative",
                                alignItems:"center",
                                boxSizing:"inherit"
                            }}>
                                                      <IconButton
           onClick={()=>router.push("/")}
              edge="start"
              style={{
                color: "#F4783B",
                marginRight:"17px",
              
              }}
            >
              <ArrowBackIosRoundedIcon style={{
               
              }} />
            </IconButton>
          <div class="jss129 jss118" style={{
              marginLeft:"0px",
              borderRadius:"50px",
              backgroundColor:"rgb(242, 242, 242)",
              position:"relative",
              width:"100%",
              boxSizing:"inherit"
          }}>
              <div class="jss130 jss119" style={{
                  width:"56px",
                  height:"100%",
                  display:"flex",
                  position:"absolute",
                  alignItems:"center",
                  pointerEvents:"none",
                  justifyContent:"center",
                  boxSizing:"inherit"
              }}>
                  <svg class="MuiSvgIcon-root" focusable="false" viewBox="0 0 24 24" 
                  aria-hidden="true" style={{
                      color:"rgb(112, 117, 133)",
                      fill:"currentcolor",
                      width:"1em",
                      height:"1em",
                      display:"inline-block",
                      fontSize:"1.5rem",
                      transition:"fill 200ms cubic-bezier(0.4, 0, 0.2, 1) 0ms",
                      flexShrink:0,
                      userSelect:"none",
                      boxSizing:"inherit",
                      overflow:"hidden"
                  }}>
                     <path d="
                     M15.5 14h-.79l-.28-.27C15.41 12.59 16 11.11 16 9.5 16 5.91 13.09 3 9.5 3S3 5.91 3 9.5 5.91 16 9.5 16c1.61 0 3.09-.59 4.23-1.57l.27.28v.79l5 4.99L20.49 19l-4.99-5zm-6 0C7.01 14 5 11.99 5 9.5S7.01 5 9.5 5 14 7.01 14 9.5 11.99 14 9.5 14z"></path>
                  </svg>
              </div>
              <div class="MuiInputBase-root jss82 jss71" style={{
                  color:"rgb(112, 117, 133)",
                  height:"40px",
                  cursor:"text",
                  display:"inline-flex",
                  position:"relative",
                  fontSize:"1rem",
                  boxSizing:"border-box",
                  alignItems:"center",
                  fontWeight:400,
                  lineHeight:"1.1876em",
              }}>
                  <input placeholder="Search…" type="text" aria-label="Search"
                  class="MuiInputBase-input jss83 jss72"  style={{
                      width:"100%",
                      padding:"8px 8px 8px 56px",
                      transition:"width 300ms cubic-bezier(0.4, 0, 0.2, 1) 0ms",
                      transitionProperty:"width",
                      transitionDuration:"300ms",
                     transitionTimingFunction: "cubic-bezier(0.4, 0, 0.2, 1)",
                     transitionDelay:"0ms",
                     font:"inherit",
                     fill:"currentcolor",
                     border:0,
                     height:"1.1876em",
                     margin:0,
                     display:"block",
                     minWidth:0,
                     background:"none",
                     boxSizing:"content-box",
                     animationName:"mui-auto-fill-cancel",
                     letterSpacing:"inherit",
                     animationDuration:"10ms",
                     WebkitTapHighlightColor:"transparent"
                  }}></input>
              </div>
          </div>
                            </div>
                        </header>
                    </div>
            <header class="MuiPaper-root MuiAppBar-root MuiAppBar-positionStatic MuiAppBar-colorPrimary jss56 MuiPaper-elevation0"
           style={{
               backgroundColor:"white",
               color:"white",
               top:"64px",
               width:"100%",
               padding:0,
               position:"fixed",
               maxWidth:"442px",
               marginBottom:"5px",
               display:"flex",
               zIndex:1100,
               boxSizing:"border-box",
               flexShrink:0,
               flexDirection:"column",
               boxShadow:"none",
               transition:"box-shadow 300ms cubic-bezier(0.4, 0, 0.2, 1) 0ms",
               fontWeight:400,
               fontSize:"0.875rem",
               lineHeight:1.43
           }} >
               <div class="MuiTabs-root-85" style={{
                   display:"flex",
                   overflow:"hidden",
                   minHeight:"48px",
                   boxSizing:"inherit",
                   color:"black"
               }}>
                   <Tabs
          value={value}
          onChange={handleChange}
          variant="scrollable"
          scrollButtons="on"
        indicatorColor="primary"
          textColor="primary"
          aria-label="scrollable force tabs example"
        >
          <Tab label="Minuman" {...a11yProps(0)}  />
         
          <Tab label="Kecantikan"  {...a11yProps(1)} />
          <Tab label="Kesehatan" {...a11yProps(2)} />
          <Tab label="Bumbu" {...a11yProps(3)} />
          <Tab label="Perlengkapan Pertanian"  {...a11yProps(4)} />
          <Tab label="Sembako"  {...a11yProps(5)} />
          <Tab label="Essential Oil" {...a11yProps(6)} />
          <Tab label="Buah" {...a11yProps(7)} />
          <Tab label="Perlengkapan Rumah Tangga" {...a11yProps(8)} />
          <Tab label="Sayuran" {...a11yProps(9)} />
          <Tab label="Snack" {...a11yProps(10)} />
          <Tab label="Lauk Pauk" {...a11yProps(11)} />
          <Tab label="Uncategorized" {...a11yProps(12)} />
        </Tabs>
        
  </div>
  
  </header>
  
  </div>
  <div  style={{
 
       paddingTop:"90px"
  }}>
 
  <TabPanel value={value} index={0} style={{    
  
  }}>

     <div class="MuiPaper-root jss20 MuiPaper-elevation0 MuiPaper-rounded" style={{
      borderRadius:0,
      backgroundColor:"#FAFAFA",
     }}>
    <div class="MuiGrid-root jss19 MuiGrid-container" style={{
      width:"100%",
      display:"flex",
      flexWrap:"wrap",
      boxSizing:"border-box"
    }}>
    <div class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-12" style={{
      padding:"0px 16px",
      backgroundColor:"rgb(250, 250, 250)",
      flexGrow:0,
      maxWidth:"100%",
      flexBasis:"100%"
    }}>
      <div class="MuiBox-root jss104 jss94" style={{
        height:"147px",
        margin:"8px -20px",
        padding:"16px",
        boxShadow:"0px 4px 8px rgb(0 0 0 / 10%)",
        borderRadius:"10px",
        backgroundColor:"#FFFFFF"
      }}>
        <div class="MuiGrid-root jss19 MuiGrid-container" style={{
      width:"100%",
      display:"flex",
      flexWrap:"wrap",
      boxSizing:"border-box"
    }}>
       <div class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-4" style={{
                flexGrow:0,
                maxWidth:"33.333333%",
                flexBasis:"33.333333%",
                boxSizing:"border-box",
                margin:0,
              }}>
      <div></div>         
              </div>
    </div>

      </div>
    </div>
    </div>
     </div>
      
      </TabPanel>
      <TabPanel value={value} index={2}>
      <div class="MuiBox-root jss141 jss131" style={{
            height:"147px",
            margin:"8px 0",
            padding:"16px",
            boxShadow:"0px 4px 8px rgb(0 0 0 / 10%)",
            borderRadius:"10px",
            backgroundColor:"#FFFFFF"
          }}>
            <div class="MuiGrid-root MuiGrid-container" style={{
              width:"100%",
              display:"flex",
              flexWrap:"wrap",
              flexBasis:"100%"
            }}>
              <div class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-4" style={{
                flexGrow:0,
                maxWidth:"33.333333%",
                flexBasis:"33.333333%",
                boxSizing:"border-box",
                margin:0,
                
              }}>
               
              </div>
              
            </div>
           
          </div>
      </TabPanel>
      <TabPanel value={value} index={3}>
      <div class="MuiBox-root jss141 jss131" style={{
            height:"147px",
            margin:"8px 0",
            padding:"16px",
            boxShadow:"0px 4px 8px rgb(0 0 0 / 10%)",
            borderRadius:"10px",
            backgroundColor:"#FFFFFF"
          }}>
            <div class="MuiGrid-root MuiGrid-container" style={{
              width:"100%",
              display:"flex",
              flexWrap:"wrap",
              flexBasis:"100%"
            }}>
              <div class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-4" style={{
                flexGrow:0,
                maxWidth:"33.333333%",
                flexBasis:"33.333333%",
                boxSizing:"border-box",
                margin:0,
                
              }}>
         
              </div>
              
            </div>
          
          </div>
      </TabPanel>
      <TabPanel value={value} index={4}>
      <div class="MuiBox-root jss141 jss131" style={{
            height:"147px",
            margin:"8px 0",
            padding:"16px",
            boxShadow:"0px 4px 8px rgb(0 0 0 / 10%)",
            borderRadius:"10px",
            backgroundColor:"#FFFFFF"
          }}>
            <div class="MuiGrid-root MuiGrid-container" style={{
              width:"100%",
              display:"flex",
              flexWrap:"wrap",
              flexBasis:"100%"
            }}>
              <div class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-4" style={{
                flexGrow:0,
                maxWidth:"33.333333%",
                flexBasis:"33.333333%",
                boxSizing:"border-box",
                margin:0,
                
              }}>
               
              </div>
              
            </div>
           
          </div>
      </TabPanel>
      <TabPanel value={value} index={5}>
        Item Six
      </TabPanel>
      <TabPanel value={value} index={7}>
      <div class="MuiBox-root jss141 jss131" style={{
            height:"147px",
            margin:"8px 0",
            padding:"16px",
            boxShadow:"0px 4px 8px rgb(0 0 0 / 10%)",
            borderRadius:"10px",
            backgroundColor:"#FFFFFF"
          }}>
            <div class="MuiGrid-root MuiGrid-container" style={{
              width:"100%",
              display:"flex",
              flexWrap:"wrap",
              flexBasis:"100%"
            }}>
              <div class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-4" style={{
                flexGrow:0,
                maxWidth:"33.333333%",
                flexBasis:"33.333333%",
                boxSizing:"border-box",
                margin:0,
                
              }}>
               
              </div>
              
            </div>
           
          </div>
      </TabPanel>
      <TabPanel value={value} index={8}>
      <div class="MuiBox-root jss141 jss131" style={{
            height:"147px",
            margin:"8px 0",
            padding:"16px",
            boxShadow:"0px 4px 8px rgb(0 0 0 / 10%)",
            borderRadius:"10px",
            backgroundColor:"#FFFFFF"
          }}>
            <div class="MuiGrid-root MuiGrid-container" style={{
              width:"100%",
              display:"flex",
              flexWrap:"wrap",
              flexBasis:"100%"
            }}>
              <div class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-4" style={{
                flexGrow:0,
                maxWidth:"33.333333%",
                flexBasis:"33.333333%",
                boxSizing:"border-box",
                margin:0,
                
              }}>
               
              </div>
              
            </div>
           
          </div>
      </TabPanel>
      <TabPanel value={value} index={9}>
      <div class="MuiBox-root jss141 jss131" style={{
            height:"147px",
            margin:"8px 0",
            padding:"16px",
            boxShadow:"0px 4px 8px rgb(0 0 0 / 10%)",
            borderRadius:"10px",
            backgroundColor:"#FFFFFF"
          }}>
            <div class="MuiGrid-root MuiGrid-container" style={{
              width:"100%",
              display:"flex",
              flexWrap:"wrap",
              flexBasis:"100%"
            }}>
              <div class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-4" style={{
                flexGrow:0,
                maxWidth:"33.333333%",
                flexBasis:"33.333333%",
                boxSizing:"border-box",
                margin:0,
                
              }}>
               
              </div>
              
            </div>
           
          </div>
      </TabPanel>
      <TabPanel value={value} index={10}>
      <div class="MuiBox-root jss141 jss131" style={{
            height:"147px",
            margin:"8px 0",
            padding:"16px",
            boxShadow:"0px 4px 8px rgb(0 0 0 / 10%)",
            borderRadius:"10px",
            backgroundColor:"#FFFFFF"
          }}>
            <div class="MuiGrid-root MuiGrid-container" style={{
              width:"100%",
              display:"flex",
              flexWrap:"wrap",
              flexBasis:"100%"
            }}>
              <div class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-4" style={{
                flexGrow:0,
                maxWidth:"33.333333%",
                flexBasis:"33.333333%",
                boxSizing:"border-box",
                margin:0,
                
              }}>
               
              </div>
              
            </div>
           
          </div>
      </TabPanel>
      <TabPanel value={value} index={11}>
      <div class="MuiBox-root jss141 jss131" style={{
            height:"147px",
            margin:"8px 0",
            padding:"16px",
            boxShadow:"0px 4px 8px rgb(0 0 0 / 10%)",
            borderRadius:"10px",
            backgroundColor:"#FFFFFF"
          }}>
            <div class="MuiGrid-root MuiGrid-container" style={{
              width:"100%",
              display:"flex",
              flexWrap:"wrap",
              flexBasis:"100%"
            }}>
              <div class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-4" style={{
                flexGrow:0,
                maxWidth:"33.333333%",
                flexBasis:"33.333333%",
                boxSizing:"border-box",
                margin:0,
                
              }}>
               
              </div>
              
            </div>
           
          </div>
      </TabPanel>
      <TabPanel value={value} index={12}>
      <div class="MuiBox-root jss141 jss131" style={{
            height:"147px",
            margin:"8px 0",
            padding:"16px",
            boxShadow:"0px 4px 8px rgb(0 0 0 / 10%)",
            borderRadius:"10px",
            backgroundColor:"#FFFFFF"
          }}>
            <div class="MuiGrid-root MuiGrid-container" style={{
              width:"100%",
              display:"flex",
              flexWrap:"wrap",
              flexBasis:"100%"
            }}>
              <div class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-4" style={{
                flexGrow:0,
                maxWidth:"33.333333%",
                flexBasis:"33.333333%",
                boxSizing:"border-box",
                margin:0,
                
              }}>
               
              </div>
              
            </div>
           
          </div>
      </TabPanel>
      
  </div>

  </main>
  </div>
    )
}
export default Category33