import React,{useEffect,useState}  from "react"
import SplashScreen from "../Component/SplashScreen"
import '../styles/globals.css'


function MyApp({ Component, pageProps }) {
  const [checked, setChecked] = useState(true);
  console.log(checked);

  useEffect(() => {
    setTimeout(() => {
      setChecked(false);
    }, 1000);
  }, []);

  if (checked === true) {
    return (
      <div style={{ width: "100%" }}>
      <SplashScreen />
    
      </div>
    );
  }
  return <Component {...pageProps} />
}

export default MyApp
