import { Typography } from "@material-ui/core"
import React from "react"
import BottomNav from "../Component/BottomNav";


function help() {
    return(
<div>
    <main class="MuiContainer-root jss199 MuiContainer-maxWidthXs" style={{
        height:"100%",
        padding:0,
        maxWidth:"444px",
        minHeight:"100vh",
        borderLeft:"1px solid #f1f1f1",
        borderRight:"1px solid #f1f1f1",
        paddingTop:"64px",
        paddingBottom:"56px",
        width:"100%",
        display:"block",
        boxSizing:"border-box",
        marginLeft:"auto",
        marginRight:"auto"
    }}>
        <div class="MuiBox-root jss216 jss207" id="box" style={{
            top:0,
            width:"100%",
            color:"white",
            zIndex:99,
            position:"fixed",
            maxWidth:"442px",
            background:"white",
            display:"flex",
            justifyContent:"center"
        }}>
            <header class="MuiPaper-root MuiAppBar-root MuiAppBar-positionStatic MuiAppBar-colorPrimary jss12 MuiPaper-elevation0" style={{
                backgroundColor:"white",
                color:"black",
                top:0,
                width:"100%",
                zIndex:999,
                position:"fixed",
                maxWidth:"442px",
                boxShadow:"0px 1px 5px rgb(0 0 0 / 5%)",
                maxHeight:"120px",

            }}>
                <div class="MuiToolbar-root MuiToolbar-dense MuiToolbar-gutters" style={{
                    minHeight:"64px",
                    paddingLeft:"16px",
                    paddingRight:"16px",
                    display:"flex",
                    position:"relative",
                    alignItems:"center"
                }}>
                    <p class="MuiTypography-root jss16 MuiTypography-subtitle1 MuiTypography-alignLeft" style={{
                        flexGrow:1,
                        textAlign:"left",
                        fontSize:"1.1rem",
                       
                        lineHeight:1.75,
                        margin:0,
                        fontFamily: "'Montserrat', sans-serif",
                        fontWeight:560
                    }}>
                        <p>Pusat Bantuan</p>
                    </p>
                </div>
            </header>
        </div>
        <div class="MuiGrid-root" elevation="0" align="center" style={{
            background:"linear-gradient(180.97deg, rgb(241, 136, 91) -22.67%, rgb(239, 112, 58) 57.41%)",
            borderRadius:"0px",
            color:"white",
            opacity:0.7,
            paddingTop:"20px",
            paddingBottom:"20px"
        }}>
              <h6 class="MuiTypography-root MuiTypography-subtitle1 MuiTypography-gutterBottom" style={{
                marginBottom:"0.35rem",
                fontWeight:300,
                lineHeight:1.75,
                margin:0,
              
                fontSize:"1.1rem",
                fontFamily:"Montserrat,sans-serif"
            }}>
                <b>Hello &nbsp;</b>
                User
            </h6>
            <h6 class="MuiTypography-root MuiTypography-h6 MuiTypography-gutterBottom" style={{
                marginBottom:"0.35em",
                fontSize:"1.25rem",
                fontWeight:549,
                lineHeight:1.6,
                margin:0,
                fontFamily:"Montserrat,sans-serif"
            }}>
                Anda Memerlukan Bantuan?
            </h6>
            </div>
            <div class="MuiGrid-root jss255 MuiGrid-container" style={{
                padding:"16px",
                background:"#FFFFFF",
                boxShadow:"0px 1px 5px rgb(0 0 0 / 5%)",
                width:"100%",
                display:"flex",
                flexWrap:"wrap",
                boxSizing:"border-box"
            }}>
                <div class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-12" style={{
                    flexGrow:0,
                    maxWidth:"100%",
                    flexBasis:"100%",
                    margin:0,
                    boxSizing:"border-box"
                }}>
                    <Typography class="MuiTypography-root jss256 MuiTypography-body1" style={{
                        fontSize:"14px",
                      fontWeight:550,
                        lineHeight:1.5,
                        margin:0,
                        boxSizing:"inherit",
                        display:"block",
                        marginBlockStart:"1em",
                        marginBlockEnd:"1em",
                        marginInlineEnd:"0px",
                        marginInlineStart:"0px",
                        fontFamily:"Montserrat,sans-serif",
                        color:"black"
                    }}><p>Tentang Kami</p></Typography>
                    <hr class="MuiDivider-root" style={{
                        marginTop: "4%",
                        border:"none",
                        height:"1px",
                        margin:0,
                        flexShrink:0,
                        backgroundColor:"rgba(0, 0, 0, 0.12)",
                        boxSizing:"inherit",
                        display:"block",
                        unicodeBidi:"isolate",
                        marginBlockStart:"0.5em",
                        marginBlockEnd:"0.5em",
                        marginInlineStart:"auto",
                        marginInlineEnd:"auto",
                        overflow:"hidden"
                    }}></hr>
                </div>
                <div class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-12" style={{
                    flexGrow:0,
                    maxWidth:"100%",
                    flexBasis:"100%",
                    margin:0,
                    boxSizing:"border-box"
                }}>
                    <p class="MuiTypography-root jss256 MuiTypography-body1" style={{
                        fontSize:"13px",
                        fontWeight:400,
                        lineHeight:1.5,
                        margin:0,
                        boxSizing:"inherit",
                        display:"block",
                        marginBlockStart:"1em",
                        marginBlockEnd:"1em",
                        marginInlineEnd:"0px",
                        marginInlineStart:"0px",
                        fontFamily:"Montserrat,sans-serif"
                    }}>Apa itu GoodPangan?</p>
                    <hr class="MuiDivider-root" style={{
                        marginTop: "4%",
                        border:"none",
                        height:"1px",
                        margin:0,
                        flexShrink:0,
                        backgroundColor:"rgba(0, 0, 0, 0.12)",
                        boxSizing:"inherit",
                        display:"block",
                        unicodeBidi:"isolate",
                        marginBlockStart:"0.5em",
                        marginBlockEnd:"0.5em",
                        marginInlineStart:"auto",
                        marginInlineEnd:"auto",
                        overflow:"hidden"
                    }}></hr>
                    </div>
                    <div class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-12" style={{
                    flexGrow:0,
                    maxWidth:"100%",
                    flexBasis:"100%",
                    margin:0,
                    boxSizing:"border-box"
                }}>
                    <Typography class="MuiTypography-root jss256 MuiTypography-body1" style={{
                        fontSize:"13px",
                        fontWeight:400,
                        lineHeight:1.5,
                        margin:0,
                        boxSizing:"inherit",
                        display:"block",
                        marginBlockStart:"1em",
                        marginBlockEnd:"1em",
                        marginInlineEnd:"0px",
                        marginInlineStart:"0px",
                        fontFamily:"Montserrat,sans-serif"
                    }}>Produk Goodpangan berasal dari mana?</Typography>
                    <hr class="MuiDivider-root" style={{
                        marginTop: "4%",
                        border:"none",
                        height:"1px",
                        margin:0,
                        flexShrink:0,
                        backgroundColor:"rgba(0, 0, 0, 0.12)",
                        boxSizing:"inherit",
                        display:"block",
                        unicodeBidi:"isolate",
                        marginBlockStart:"0.5em",
                        marginBlockEnd:"0.5em",
                        marginInlineStart:"auto",
                        marginInlineEnd:"auto",
                        overflow:"hidden"
                    }}></hr>
                    </div>
                    <div class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-12" style={{
                    flexGrow:0,
                    maxWidth:"100%",
                    flexBasis:"100%",
                    margin:0,
                    boxSizing:"border-box"
                }}>
                    <Typography class="MuiTypography-root jss256 MuiTypography-body1" style={{
                        fontSize:"13px",
                        fontWeight:400,
                        lineHeight:1.5,
                        margin:0,
                        boxSizing:"inherit",
                        display:"block",
                        marginBlockStart:"1em",
                        fontFamily:"Montserrat,sans-serif"
                        // marginBlockEnd:"1em",
                        // marginInlineEnd:"0px",
                        // marginInlineStart:"0px"
                    }}>Apa standar produksi para petani Goodpangan?</Typography>
                    </div> 
            </div>
        <div class="MuiGrid-root jss374 MuiGrid-container" style={{
            padding:"16px",
            background:"#FFFFFF",
            boxShadow:"0px 1px 5px rgb(0 0 0 / 5%)",
            marginTop:"3%",
            width:"100%",
            display:"flex",
            flexWrap:"wrap",
            boxSizing:"border-box"
        }}>
            <div class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-12" style={{
                    flexGrow:0,
                    maxWidth:"100%",
                    flexBasis:"100%",
                    margin:0,
                    boxSizing:"border-box"
                }}>
                    <Typography class="MuiTypography-root jss256 MuiTypography-body1" style={{
                        fontSize:"14px",
                        fontWeight:400,
                        lineHeight:1.5,
                        margin:0,
                        boxSizing:"inherit",
                        display:"block",
                        marginBlockStart:"1em",
                        marginBlockEnd:"1em",
                        marginInlineEnd:"0px",
                        marginInlineStart:"0px",
                        fontFamily:"Montserrat,sans-serif"
                    }}><b>Akun</b></Typography>
                    <hr class="MuiDivider-root" style={{
                        marginTop: "4%",
                        border:"none",
                        height:"1px",
                        margin:0,
                        flexShrink:0,
                        backgroundColor:"rgba(0, 0, 0, 0.12)",
                        boxSizing:"inherit",
                        display:"block",
                        unicodeBidi:"isolate",
                        marginBlockStart:"0.5em",
                        marginBlockEnd:"0.5em",
                        marginInlineStart:"auto",
                        marginInlineEnd:"auto",
                        overflow:"hidden"
                    }}></hr>
                </div>
                <div class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-12" style={{
                    flexGrow:0,
                    maxWidth:"100%",
                    flexBasis:"100%",
                    margin:0,
                    boxSizing:"border-box"
                }}>
                    <Typography class="MuiTypography-root jss256 MuiTypography-body1" style={{
                        fontSize:"13px",
                        fontWeight:400,
                        lineHeight:1.5,
                        margin:0,
                        boxSizing:"inherit",
                        display:"block",
                        marginBlockStart:"1em",
                        marginBlockEnd:"1em",
                        marginInlineEnd:"0px",
                        marginInlineStart:"0px",
                        fontFamily:"Montserrat,sans-serif"
                    }}>Bagaimana cara login di akun Goodpangan?</Typography>
                    </div>
        </div>
        <div class="MuiGrid-root jss374 MuiGrid-container" style={{
            padding:"16px",
            background:"#FFFFFF",
            boxShadow:"0px 1px 5px rgb(0 0 0 / 5%)",
            marginTop:"3%",
            width:"100%",
            display:"flex",
            flexWrap:"wrap",
            boxSizing:"border-box"
        }}>
            <div class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-12" style={{
                    flexGrow:0,
                    maxWidth:"100%",
                    flexBasis:"100%",
                    margin:0,
                    boxSizing:"border-box"
                }}>
                    <Typography class="MuiTypography-root jss256 MuiTypography-body1" style={{
                        fontSize:"14px",
                        fontWeight:400,
                        lineHeight:1.5,
                        margin:0,
                        boxSizing:"inherit",
                        display:"block",
                        marginBlockStart:"1em",
                        marginBlockEnd:"1em",
                        marginInlineEnd:"0px",
                        marginInlineStart:"0px",
                        fontFamily:"Montserrat,sans-serif"
                    }}><b>Operasional</b></Typography>
                    <hr class="MuiDivider-root" style={{
                        marginTop: "4%",
                        border:"none",
                        height:"1px",
                        margin:0,
                        flexShrink:0,
                        backgroundColor:"rgba(0, 0, 0, 0.12)",
                        boxSizing:"inherit",
                        display:"block",
                        unicodeBidi:"isolate",
                        marginBlockStart:"0.5em",
                        marginBlockEnd:"0.5em",
                        marginInlineStart:"auto",
                        marginInlineEnd:"auto",
                        overflow:"hidden"
                    }}></hr>
                </div>
                <div class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-12" style={{
                    flexGrow:0,
                    maxWidth:"100%",
                    flexBasis:"100%",
                    margin:0,
                    boxSizing:"border-box"
                }}>
                    <Typography class="MuiTypography-root jss256 MuiTypography-body1" style={{
                        fontSize:"13px",
                        fontWeight:400,
                        lineHeight:1.5,
                        margin:0,
                        boxSizing:"inherit",
                        display:"block",
                        marginBlockStart:"1em",
                        marginBlockEnd:"1em",
                        marginInlineEnd:"0px",
                        marginInlineStart:"0px",
                        fontFamily:"Montserrat,sans-serif"
                    }}>Operasional Goodpangan sudah ada di kota mana saja?</Typography>
                    </div>
        </div>
        <div class="MuiGrid-root jss374 MuiGrid-container" style={{
            padding:"16px",
            background:"#FFFFFF",
            boxShadow:"0px 1px 5px rgb(0 0 0 / 5%)",
            marginTop:"3%",
            width:"100%",
            display:"flex",
            flexWrap:"wrap",
            boxSizing:"border-box"
        }}>
               <div class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-12" style={{
                    flexGrow:0,
                    maxWidth:"100%",
                    flexBasis:"100%",
                    margin:0,
                    boxSizing:"border-box"
                }}>
                    <Typography class="MuiTypography-root jss256 MuiTypography-body1" style={{
                        fontSize:"14px",
                        fontWeight:400,
                        lineHeight:1.5,
                        margin:0,
                        boxSizing:"inherit",
                        display:"block",
                        marginBlockStart:"1em",
                        marginBlockEnd:"1em",
                        marginInlineEnd:"0px",
                        marginInlineStart:"0px",
                        fontFamily:"Montserrat,sans-serif"
                    }}><b>Harga dan Transaksi</b></Typography>
                    <hr class="MuiDivider-root" style={{
                        marginTop: "4%",
                        border:"none",
                        height:"1px",
                        margin:0,
                        flexShrink:0,
                        backgroundColor:"rgba(0, 0, 0, 0.12)",
                        boxSizing:"inherit",
                        display:"block",
                        unicodeBidi:"isolate",
                        marginBlockStart:"0.5em",
                        marginBlockEnd:"0.5em",
                        marginInlineStart:"auto",
                        marginInlineEnd:"auto",
                        overflow:"hidden"
                    }}></hr>
                    </div>
                    <div class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-12" style={{
                    flexGrow:0,
                    maxWidth:"100%",
                    flexBasis:"100%",
                    margin:0,
                    boxSizing:"border-box"
                }}>
                    <Typography class="MuiTypography-root jss256 MuiTypography-body1" style={{
                        fontSize:"14px",
                        fontWeight:400,
                        lineHeight:1.5,
                        margin:0,
                        boxSizing:"inherit",
                        display:"block",
                        marginBlockStart:"1em",
                        marginBlockEnd:"1em",
                        marginInlineEnd:"0px",
                        marginInlineStart:"0px",
                        fontFamily:"Montserrat,sans-serif"
                    }}>Berapa ongkos kirim nya?</Typography>
                    <hr class="MuiDivider-root" style={{
                        marginTop: "4%",
                        border:"none",
                        height:"1px",
                        margin:0,
                        flexShrink:0,
                        backgroundColor:"rgba(0, 0, 0, 0.12)",
                        boxSizing:"inherit",
                        display:"block",
                        unicodeBidi:"isolate",
                        marginBlockStart:"0.5em",
                        marginBlockEnd:"0.5em",
                        marginInlineStart:"auto",
                        marginInlineEnd:"auto",
                        overflow:"hidden"
                    }}></hr>
                    </div>
                    <div class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-12" style={{
                    flexGrow:0,
                    maxWidth:"100%",
                    flexBasis:"100%",
                    margin:0,
                    boxSizing:"border-box"
                }}>
                    <Typography class="MuiTypography-root jss256 MuiTypography-body1" style={{
                        fontSize:"13px",
                        fontWeight:400,
                        lineHeight:1.5,
                        margin:0,
                        boxSizing:"inherit",
                        display:"block",
                        marginBlockStart:"1em",
                        marginBlockEnd:"1em",
                        marginInlineEnd:"0px",
                        marginInlineStart:"0px",
                        fontFamily:"Montserrat,sans-serif"
                    }}>Bagaimana saya dapat kembalian uang?</Typography>
                    </div>          
        </div>
        <div class="MuiGrid-root jss374 MuiGrid-container" style={{
            padding:"16px",
            background:"#FFFFFF",
            boxShadow:"0px 1px 5px rgb(0 0 0 / 5%)",
            marginTop:"3%",
            width:"100%",
            display:"flex",
            flexWrap:"wrap",
            boxSizing:"border-box"
        }}>
               <div class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-12" style={{
                    flexGrow:0,
                    maxWidth:"100%",
                    flexBasis:"100%",
                    margin:0,
                    boxSizing:"border-box"
                }}>
                    <Typography class="MuiTypography-root jss256 MuiTypography-body1" style={{
                        fontSize:"14px",
                        fontWeight:400,
                        lineHeight:1.5,
                        margin:0,
                        boxSizing:"inherit",
                        display:"block",
                        marginBlockStart:"1em",
                        marginBlockEnd:"1em",
                        marginInlineEnd:"0px",
                        marginInlineStart:"0px",
                        fontFamily:"Montserrat,sans-serif"
                    }}><b>Pesanan</b></Typography>
                    <hr class="MuiDivider-root" style={{
                        marginTop: "4%",
                        border:"none",
                        height:"1px",
                        margin:0,
                        flexShrink:0,
                        backgroundColor:"rgba(0, 0, 0, 0.12)",
                        boxSizing:"inherit",
                        display:"block",
                        unicodeBidi:"isolate",
                        marginBlockStart:"0.5em",
                        marginBlockEnd:"0.5em",
                        marginInlineStart:"auto",
                        marginInlineEnd:"auto",
                        overflow:"hidden"
                    }}></hr>
                    </div>
                    <div class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-12" style={{
                    flexGrow:0,
                    maxWidth:"100%",
                    flexBasis:"100%",
                    margin:0,
                    boxSizing:"border-box"
                }}>
                    <Typography class="MuiTypography-root jss256 MuiTypography-body1" style={{
                        fontSize:"13px",
                        fontWeight:400,
                        lineHeight:1.5,
                        margin:0,
                        boxSizing:"inherit",
                        display:"block",
                        marginBlockStart:"1em",
                        marginBlockEnd:"1em",
                        marginInlineEnd:"0px",
                        marginInlineStart:"0px",
                        fontFamily:"Montserrat,sans-serif"
                    }}>Seberapa cepat Goodpangan mengantarkan pesanan?</Typography>
                    <hr class="MuiDivider-root" style={{
                        marginTop: "4%",
                        border:"none",
                        height:"1px",
                        margin:0,
                        flexShrink:0,
                        backgroundColor:"rgba(0, 0, 0, 0.12)",
                        boxSizing:"inherit",
                        display:"block",
                        unicodeBidi:"isolate",
                        marginBlockStart:"0.5em",
                        marginBlockEnd:"0.5em",
                        marginInlineStart:"auto",
                        marginInlineEnd:"auto",
                        overflow:"hidden"
                    }}></hr>
                    </div>
                    <div class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-12" style={{
                    flexGrow:0,
                    maxWidth:"100%",
                    flexBasis:"100%",
                    margin:0,
                    boxSizing:"border-box"
                }}>
                    <Typography class="MuiTypography-root jss256 MuiTypography-body1" style={{
                        fontSize:"13px",
                        fontWeight:400,
                        lineHeight:1.5,
                        margin:0,
                        boxSizing:"inherit",
                        display:"block",
                        marginBlockStart:"1em",
                        marginBlockEnd:"1em",
                        marginInlineEnd:"0px",
                        marginInlineStart:"0px",
                        fontFamily:"Montserrat,sans-serif"
                    }}>Apa yang dimaksud dengan waktu pengantaran?</Typography>
                      <hr class="MuiDivider-root" style={{
                        marginTop: "4%",
                        border:"none",
                        height:"1px",
                        margin:0,
                        flexShrink:0,
                        backgroundColor:"rgba(0, 0, 0, 0.12)",
                        boxSizing:"inherit",
                        display:"block",
                        unicodeBidi:"isolate",
                        marginBlockStart:"0.5em",
                        marginBlockEnd:"0.5em",
                        marginInlineStart:"auto",
                        marginInlineEnd:"auto",
                        overflow:"hidden"
                    }}></hr>
                    </div>     
                    <div class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-12" style={{
                    flexGrow:0,
                    maxWidth:"100%",
                    flexBasis:"100%",
                    margin:0,
                    boxSizing:"border-box"
                }}>
                    <Typography class="MuiTypography-root jss256 MuiTypography-body1" style={{
                        fontSize:"13px",
                        fontWeight:400,
                        lineHeight:1.5,
                        margin:0,
                        boxSizing:"inherit",
                        display:"block",
                        marginBlockStart:"1em",
                        marginBlockEnd:"1em",
                        marginInlineEnd:"0px",
                        marginInlineStart:"0px"
                    }}>Bagaimana saya mengecek status pesanan saya?</Typography>
                      <hr class="MuiDivider-root" style={{
                        marginTop: "4%",
                        border:"none",
                        height:"1px",
                        margin:0,
                        flexShrink:0,
                        backgroundColor:"rgba(0, 0, 0, 0.12)",
                        boxSizing:"inherit",
                        display:"block",
                        unicodeBidi:"isolate",
                        marginBlockStart:"0.5em",
                        marginBlockEnd:"0.5em",
                        marginInlineStart:"auto",
                        marginInlineEnd:"auto",
                        overflow:"hidden"
                    }}></hr>   
                    </div>
                    <div class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-12" style={{
                    flexGrow:0,
                    maxWidth:"100%",
                    flexBasis:"100%",
                    margin:0,
                    boxSizing:"border-box"
                }}>
                    <Typography class="MuiTypography-root jss256 MuiTypography-body1" style={{
                        fontSize:"13px",
                        fontWeight:400,
                        lineHeight:1.5,
                        margin:0,
                        boxSizing:"inherit",
                        display:"block",
                        marginBlockStart:"1em",
                        marginBlockEnd:"1em",
                        marginInlineEnd:"0px",
                        marginInlineStart:"0px",
                        fontFamily:"Montserrat,sans-serif"
                    }}>Bagaimana cara merubah atau membatalkan pesanan?</Typography>
                      <hr class="MuiDivider-root" style={{
                        marginTop: "4%",
                        border:"none",
                        height:"1px",
                        margin:0,
                        flexShrink:0,
                        backgroundColor:"rgba(0, 0, 0, 0.12)",
                        boxSizing:"inherit",
                        display:"block",
                        unicodeBidi:"isolate",
                        marginBlockStart:"0.5em",
                        marginBlockEnd:"0.5em",
                        marginInlineStart:"auto",
                        marginInlineEnd:"auto",
                        overflow:"hidden"
                    }}></hr>
                    </div>  
                    <div class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-12" style={{
                    flexGrow:0,
                    maxWidth:"100%",
                    flexBasis:"100%",
                    margin:0,
                    boxSizing:"border-box"
                }}>
                    <Typography class="MuiTypography-root jss256 MuiTypography-body1" style={{
                        fontSize:"13px",
                        fontWeight:400,
                        lineHeight:1.5,
                        margin:0,
                        boxSizing:"inherit",
                        display:"block",
                        marginBlockStart:"1em",
                        marginBlockEnd:"1em",
                        marginInlineEnd:"0px",
                        marginInlineStart:"0px",
                        fontFamily:"Montserrat,sans-serif"
                    }}>Apa kebijakan terkait pembatalan pesanan?</Typography>
                      <hr class="MuiDivider-root" style={{
                        marginTop: "4%",
                        border:"none",
                        height:"1px",
                        margin:0,
                        flexShrink:0,
                        backgroundColor:"rgba(0, 0, 0, 0.12)",
                        boxSizing:"inherit",
                        display:"block",
                        unicodeBidi:"isolate",
                        marginBlockStart:"0.5em",
                        marginBlockEnd:"0.5em",
                        marginInlineStart:"auto",
                        marginInlineEnd:"auto",
                        overflow:"hidden"
                    }}></hr>
                    </div>
                    <div class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-12" style={{
                    flexGrow:0,
                    maxWidth:"100%",
                    flexBasis:"100%",
                    margin:0,
                    boxSizing:"border-box"
                }}>
                    <Typography class="MuiTypography-root jss256 MuiTypography-body1" style={{
                        fontSize:"13px",
                        fontWeight:400,
                        lineHeight:1.5,
                        margin:0,
                        boxSizing:"inherit",
                        display:"block",
                        marginBlockStart:"1em",
                        marginBlockEnd:"1em",
                        marginInlineEnd:"0px",
                        marginInlineStart:"0px",
                        fontFamily:"Montserrat,sans-serif"
                    }}>Bagaimana Proses Ganti Rugi Jika Barang Rusak/Hilang Dalam pengiriman?</Typography>
                      <hr class="MuiDivider-root" style={{
                        marginTop: "4%",
                        border:"none",
                        height:"1px",
                        margin:0,
                        flexShrink:0,
                        backgroundColor:"rgba(0, 0, 0, 0.12)",
                        boxSizing:"inherit",
                        display:"block",
                        unicodeBidi:"isolate",
                        marginBlockStart:"0.5em",
                        marginBlockEnd:"0.5em",
                        marginInlineStart:"auto",
                        marginInlineEnd:"auto",
                        overflow:"hidden"
                    }}></hr>
                    </div>
                    
                    <div class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-12" style={{
                    flexGrow:0,
                    maxWidth:"100%",
                    flexBasis:"100%",
                    margin:0,
                    boxSizing:"border-box"
                }}>
                    <Typography class="MuiTypography-root jss256 MuiTypography-body1" style={{
                        fontSize:"13px",
                        fontWeight:400,
                        lineHeight:1.5,
                        margin:0,
                        boxSizing:"inherit",
                        display:"block",
                        marginBlockStart:"1em",
                        marginBlockEnd:"1em",
                        marginInlineEnd:"0px",
                        marginInlineStart:"0px",
                        fontFamily:"Montserrat,sans-serif"
                    }}>Saya punya pertanyaan lebih lanjut untuk Goodpangan!</Typography>
                      <hr class="MuiDivider-root" style={{
                        marginTop: "4%",
                        border:"none",
                        height:"1px",
                        margin:0,
                        flexShrink:0,
                        backgroundColor:"rgba(0, 0, 0, 0.12)",
                        boxSizing:"inherit",
                        display:"block",
                        unicodeBidi:"isolate",
                        marginBlockStart:"0.5em",
                        marginBlockEnd:"0.5em",
                        marginInlineStart:"auto",
                        marginInlineEnd:"auto",
                        overflow:"hidden"
                    }}></hr>
                    </div>
                    <div class="MuiGrid-root jss373 MuiGrid-item MuiGrid-grid-xs-12" style={{
                        marginTop:"16px",
                        flexGrow:0,
                        maxWidth:"100%",
                        flexBasis:"100%",
                        margin:0,
                        boxSizing:"border-box"
                    }}>
                        <Typography class="MuiTypography-root jss469 MuiTypography-body1" style={{
                             fontSize:"13px",
                             fontWeight:400,
                             lineHeight:1.5,
                             margin:0,
                             boxSizing:"inherit",
                             display:"block",
                             marginBlockStart:"1em",
                             marginBlockEnd:"1em",
                             marginInlineEnd:"0px",
                             marginInlineStart:"0px",
                             fontFamily:"Montserrat,sans-serif"
                        }}>Masih <b>butuh bantuan</b> atau <b>punya pertanyaan lain</b>&nbsp; yang ingin ditanyakan?  <b style={{
                            fontWeight:700,
                            color:"rgb(244, 120, 59)",
                            fontFamily:"Montserrat,sans-serif"
                        }}>HUBUNGI KAMI </b></Typography>
                    </div>
        </div>
        <div class="MuiGrid-root MuiGrid-container" style={{
            padding:"10px",
            width:"100%",
            display:"flex",
            flexWrap:"wrap",
            boxSizing:"border-box"
        }}>
            <div class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-1" style={{
                display:"flex",
                justifyContent:"center",
                marginTop:"-5%",
                flexGrow:0,
                maxWidth:"8.333333%",
                flexBasis:"8.333333%",
                margin:0,
                boxSizing:"border-box"
            }}>
                <img src="https://goodpangan.com/static/media/info.f0b746a7.svg"></img>
            </div>
            <img
            alt=""
            src="https://goodpangan.com/media/wa.4c0c2a92.svg"
            style={{
              bottom: "16px",
              marginBottom: "56px",
              position: "fixed",
              right: "0"
            }}
          />
            <div class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-11" style={{
                flexGrow:0,
                maxWidth:"91.666667%",
                flexBasis:"91.666667%",
                margin:0,
                boxSizing:"border-box",
                display:"block"
            }}>
                <span class="MuiTypography-root MuiTypography-caption MuiTypography-gutterBottom MuiTypography-displayBlock" style={{
                      color:"rgb(137, 139, 140)",
                      fontWeight:"bold",
                      display:"block",
                      marginBottom:"0.35em",
                      lineHeight:1.66,
                      margin:0,
                      boxSizing:"inherit",
                      fontFamily:"Montserrat,sans-serif"
                }}>Layanan Pelanggan 24 Jam, Senin s/d Minggu, tidak termasuk Hari Libur Nasional.</span>
            </div>
        </div>
        <div style={{
            marginLeft:"74%",
        }}>
           <a target="_blank" rel="noreferrer" href="https://api.whatsapp.com/send?phone=6281228823048&text=Hai GoodPangan, mau tanya dong" 
           style={{
               color:"rgb(241, 91, 93)",
               textDecoration:"none",
               boxSizing:"inherit"
           }}>
            <img src="https://goodpangan.com/static/media/wa.4c0c2a92.svg" style={{
                cursor:"pointer",
                marginBottom:"56px",
                bottom:"16px",
                position:"fixed",
                boxSizing:"inherit"
            }}></img>
            </a>
        </div>
       </main>
       <BottomNav/>
       </div>
     
    )
}
export default help;