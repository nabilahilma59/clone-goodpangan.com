
import React from "react";
import { withStyles } from "@material-ui/styles";
import styles from "../Component/styleCard.js"
import { useRouter} from "next/router";
function Category(props) {
    const router = useRouter()
const {classes} = props
    return(
       <div>
           <div class="jss7" style={{
               width:"100%",
               padding:"16px",
               marginTop:"8px",
               backgroundColor:"#ffffff",
                   
           }}>
               
<div class="jss8" style={{
    display:"flex",
    flexWrap:"wrap",
    alignContent:"flex-start",
    flexDirection:"row",
    justifyContent:"flex-start"
}}>
<div style={{
width:"20%"
}}>
    <div class="MuiGrid-root jss33 MuiGrid-item" style={{
        cursor:"pointer",
        display:"flex",
        textAlign:"center",
        alignItems:"center",
        marginBottom:"6px",
        flexDirection:"column",
        boxShadow: "10px 38px 78px -32px rgba(0,0,0,0.75)" 
    }}>
        <div class="jss31" className={classes.boxCategories} style={{
            width:"64px",
            height:"64px",
            display:"flex",
            padding:"14px",
            transition:"all 0.3s ease 0s",
            alignItems:"center",
            marginLeft:"8px",
            marginRight:"8px",
            borderRadius:"12px",
            marginBottom:"6px",
            justifyContent:"center",
            backgroundColor:"#FFECE2",
           
            
        }}>
            <img  onClick={()=>router.push("/category33")} src="https://dashboard.goodpangan.com/wp-content/uploads/2020/07/drink-water-1.png" style={{
                width:"40px",
                height:"40px",
            }}></img>
        </div>
        <span   class="jss34" style={{
            color:"#707585",
            fontSize:"10px",
            textAlign:"center",
            fontWeight:"bold"
        }}>Minuman</span>
        </div>

    </div>
    <div style={{
width:"20%"
}}>
    <div class="MuiGrid-root jss33 MuiGrid-item" style={{
        cursor:"pointer",
        display:"flex",
        textAlign:"center",
        alignItems:"center",
        marginBottom:"6px",
        flexDirection:"column",
        boxShadow: "10px 38px 78px -32px rgba(0,0,0,0.75)" 
    }}>
        <div class="jss31" className={classes.boxCategories} style={{
            width:"64px",
            height:"64px",
            display:"flex",
            padding:"14px",
            transition:"all 0.3s ease 0s",
            alignItems:"center",
            marginLeft:"8px",
            marginRight:"8px",
            borderRadius:"12px",
            marginBottom:"6px",
            justifyContent:"center",
            backgroundColor:"#FFECE2",
           
            
        }}>
            <img src="https://dashboard.goodpangan.com/wp-content/uploads/2020/07/skin.png" style={{
                width:"40px",
                height:"40px",
            }}></img>
        </div>
        <span class="jss34" style={{
            color:"#707585",
            fontSize:"10px",
            textAlign:"center",
            fontWeight:"bold"
        }}>Kecantikan</span>
        </div>

    </div>
    <div style={{
width:"20%"
}}>
    <div class="MuiGrid-root jss33 MuiGrid-item" style={{
        cursor:"pointer",
        display:"flex",
        textAlign:"center",
        alignItems:"center",
        marginBottom:"6px",
        flexDirection:"column",
        boxShadow: "10px 38px 78px -32px rgba(0,0,0,0.75)" 
    }}>
        <div class="jss31" className={classes.boxCategories} style={{
            width:"64px",
            height:"64px",
            display:"flex",
            padding:"14px",
            transition:"all 0.3s ease 0s",
            alignItems:"center",
            marginLeft:"8px",
            marginRight:"8px",
            borderRadius:"12px",
            marginBottom:"6px",
            justifyContent:"center",
            backgroundColor:"#FFECE2",
           
            
        }}>
            <img src="https://dashboard.goodpangan.com/wp-content/uploads/2020/07/hospital.png" style={{
                width:"40px",
                height:"40px",
            }}></img>
        </div>
        <span class="jss34" style={{
            color:"#707585",
            fontSize:"10px",
            textAlign:"center",
            fontWeight:"bold"
        }}>Kesehatan</span>
        </div>

    </div>
    <div style={{
width:"20%"
}}>
    <div class="MuiGrid-root jss33 MuiGrid-item" style={{
        cursor:"pointer",
        display:"flex",
        textAlign:"center",
        alignItems:"center",
        marginBottom:"6px",
        flexDirection:"column",
        boxShadow: "10px 38px 78px -32px rgba(0,0,0,0.75)" 
    }}>
        <div class="jss31" className={classes.boxCategories} style={{
            width:"64px",
            height:"64px",
            display:"flex",
            padding:"14px",
            transition:"all 0.3s ease 0s",
            alignItems:"center",
            marginLeft:"8px",
            marginRight:"8px",
            borderRadius:"12px",
            marginBottom:"6px",
            justifyContent:"center",
            backgroundColor:"#FFECE2",
           
            
        }}>
            <img src="https://dashboard.goodpangan.com/wp-content/uploads/2020/07/021-garlic-1.png" style={{
                width:"40px",
                height:"40px",
            }}></img>
        </div>
        <span class="jss34" style={{
            color:"#707585",
            fontSize:"10px",
            textAlign:"center",
            fontWeight:"bold"
        }}>Bumbu</span>
        </div>

    </div>
    <div style={{
width:"20%"
}}>
    <div class="MuiGrid-root jss33 MuiGrid-item" style={{
        cursor:"pointer",
        display:"flex",
        textAlign:"center",
        alignItems:"center",
        marginBottom:"6px",
        flexDirection:"column",
        boxShadow: "10px 38px 78px -32px rgba(0,0,0,0.75)" 
    }}>
        <div class="jss31" className={classes.boxCategories} style={{
            width:"64px",
            height:"64px",
            display:"flex",
            padding:"14px",
            transition:"all 0.3s ease 0s",
            alignItems:"center",
            marginLeft:"8px",
            marginRight:"8px",
            borderRadius:"12px",
            marginBottom:"6px",
            justifyContent:"center",
            backgroundColor:"#FFECE2",
           
            
        }}>
            <img src="https://dashboard.goodpangan.com/wp-content/uploads/2020/07/soil.png" style={{
                width:"40px",
                height:"40px",
            }}></img>
        </div>
        <span class="jss34" style={{
            color:"#707585",
            fontSize:"10px",
            textAlign:"center",
            fontWeight:"bold"
        }}>Perlengkapan Pertanian</span>
        </div>
    </div>
</div>

     <div class="jss8" style={{
         marginTop:"12px",
         display:"flex",
         flexWrap:"wrap",
         alignContent:"flexStart",
         flexDirection:"row"
     }}>
         <div style={{
             width:"20%",
         }}>
             <div class="MuiGrid-root jss33 MuiGrid-item" style={{
                 cursor:"pointer",
                 display:"flex",
                 textAlign:"center",
                 alignItems:"center",
                 marginBottom:"6px",
                 flexDirection:"column",
                 margin:0,
                 boxSizing:"borderbox",
                 boxShadow: "10px 38px 78px -32px rgba(0,0,0,0.75)" 
             }}>
                 <div class="jss31" className={classes.boxCategories} style={{
            width:"64px",
            height:"64px",
            display:"flex",
            padding:"14px",
            transition:"all 0.3s ease 0s",
            alignItems:"center",
            marginLeft:"8px",
            marginRight:"8px",
            borderRadius:"12px",
            marginBottom:"6px",
            justifyContent:"center",
            backgroundColor:"#FFECE2",
        }}>
            <img src="https://dashboard.goodpangan.com/wp-content/uploads/2020/07/003-pea-1.png" style={{
                width:"40px",
                height:"40px",
            }}></img>
        </div>
        <span class="jss34" style={{
            color:"#707585",
            fontSize:"10px",
            textAlign:"center",
            fontWeight:"bold"
        }}>Sembako</span>
        </div>

             </div>
             <div style={{
                width:"20%"
             }}>
                 <div class="MuiGrid-root jss33 MuiGrid-item" style={{
                     cursor:"pointer",
                     display:"flex",
                     textAlign:"center",
                     alignItems:"center",
                     marginBottom:"6px",
                     flexDirection:"column",
                     margin:0,
                     boxSizing:"border-box",
                     boxShadow: "10px 38px 78px -32px rgba(0,0,0,0.75)" 
                 }}>
                      <div class="jss31" className={classes.boxCategories} style={{
            width:"64px",
            height:"64px",
            display:"flex",
            padding:"14px",
            transition:"all 0.3s ease 0s",
            alignItems:"center",
            marginLeft:"8px",
            marginRight:"8px",
            borderRadius:"12px",
            marginBottom:"6px",
            justifyContent:"center",
            backgroundColor:"#FFECE2",
        }}>
            <img src="https://dashboard.goodpangan.com/wp-content/uploads/2020/07/aromatherapy-1.png" style={{
                 width:"40px",
                 height:"40px",
            }}></img>
        </div>
        <span class="jss34" style={{
            color:"#707585",
            fontSize:"10px",
            textAlign:"center",
            fontWeight:"bold"
        }}>Essential oil</span>
                 </div>
             </div>
             <div style={{
                width:"20%"
             }}>
                 <div class="MuiGrid-root jss33 MuiGrid-item" style={{
                     cursor:"pointer",
                     display:"flex",
                     textAlign:"center",
                     alignItems:"center",
                     marginBottom:"6px",
                     flexDirection:"column",
                     margin:0,
                     boxSizing:"border-box",
                     boxShadow: "10px 38px 78px -32px rgba(0,0,0,0.75)" 
                 }}>
                      <div class="jss31" className={classes.boxCategories} style={{
            width:"64px",
            height:"64px",
            display:"flex",
            padding:"14px",
            transition:"all 0.3s ease 0s",
            alignItems:"center",
            marginLeft:"8px",
            marginRight:"8px",
            borderRadius:"12px",
            marginBottom:"6px",
            justifyContent:"center",
            backgroundColor:"#FFECE2",
        }}>
            <img src="https://dashboard.goodpangan.com/wp-content/uploads/2020/07/050-apple-1-1.png" style={{
                 width:"40px",
                 height:"40px",
            }}></img>
        </div>
        <span class="jss34" style={{
            color:"#707585",
            fontSize:"10px",
            textAlign:"center",
            fontWeight:"bold"
        }}>Buah</span>
                 </div>
             </div>
             <div style={{
                width:"20%"
             }}>
                 <div class="MuiGrid-root jss33 MuiGrid-item" style={{
                     cursor:"pointer",
                     display:"flex",
                     textAlign:"center",
                     alignItems:"center",
                     marginBottom:"6px",
                     flexDirection:"column",
                     margin:0,
                     boxSizing:"border-box",
                     boxShadow: "10px 38px 78px -32px rgba(0,0,0,0.75)" 
                 }}>
                      <div class="jss31" className={classes.boxCategories} style={{
            width:"64px",
            height:"64px",
            display:"flex",
            padding:"14px",
            transition:"all 0.3s ease 0s",
            alignItems:"center",
            marginLeft:"8px",
            marginRight:"8px",
            borderRadius:"12px",
            marginBottom:"6px",
            justifyContent:"center",
            backgroundColor:"#FFECE2",
        }}>
            <img src="https://dashboard.goodpangan.com/wp-content/uploads/2020/10/WhatsApp-Image-2020-10-12-at-12.45.42.jpeg"
            alt="Categories Logo"  style={{
                 width:"40px",
                 height:"40px",
            }}></img>
        </div>
        <span class="jss34" style={{
            color:"#707585",
            fontSize:"10px",
            textAlign:"center",
            fontWeight:"bold"
        }}>Perlengkapan Rumah Tangga</span>
                 </div>
             </div>
             <div style={{
                width:"20%"
             }}>
                 <div class="MuiGrid-root jss33 MuiGrid-item" style={{
                     cursor:"pointer",
                     display:"flex",
                     textAlign:"center",
                     alignItems:"center",
                     marginBottom:"6px",
                     flexDirection:"column",
                     margin:0,
                     boxSizing:"border-box",
                     boxShadow: "10px 38px 78px -32px rgba(0,0,0,0.75)" 
                 }}>
                      <div class="jss31" className={classes.boxCategories} style={{
            width:"64px",
            height:"64px",
            display:"flex",
            padding:"14px",
            transition:"all 0.3s ease 0s",
            alignItems:"center",
            marginLeft:"8px",
            marginRight:"8px",
            borderRadius:"12px",
            marginBottom:"6px",
            justifyContent:"center",
            backgroundColor:"#FFECE2",
        }}>
            <img src="https://goodpangan.com/static/media/other.42570746.svg" style={{
                 width:"40px",
                 height:"40px",
            }}></img>
        </div>
        <span class="jss34" style={{
            color:"#707585",
            fontSize:"10px",
            textAlign:"center",
            fontWeight:"bold"
        }}>Lainnya</span>
                 </div>
             </div>
             
             
         </div>
     </div>
        
          </div>
      
       
    )
}
export default  withStyles(styles)(Category);