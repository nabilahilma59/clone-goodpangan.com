import React from "react";
import Typography from "@material-ui/core/Typography";
import Promo from "../Component/Promo";
import Category from "../Component/Category";
import PilihanHemat from "./PilihanHemat"
import BottomNav from "../Component/BottomNav";
import { useRouter} from "next/router";

function AppBar() {
  const router = useRouter()
    return(
   <div>
        <div>
        <div class="MuiContainer-root jss24 MuiContainer-maxWidthXs" style={{
          backgroundColor:"#FAFAFA",
          height:"100%",
          padding:0,
          maxWidth:"444px",
          minHeight:"100vh",
          borderLeft:"1px solid #f1f1f1",
          borderRight:"1px solid #f1f1f1",
          paddingBottom:"56px",
          width:"100%",
          display:"block",
          boxSizing:"border-box",
          marginRight:"auto",
          marginLeft:"auto"
        }}>
          <div style={{
            position:"fixed",
            maxWidth:"442px",
            top:0,
            zIndex:99,
            height:"150px",
            width:"100%",
            backgroundColor:"rgb(255, 255, 255)",
            boxShadow:"rgb(0 0 0 / 10%) 0px 4px 4px"
          }}>
            <div class="MuiGrid-root jss38 MuiGrid-container MuiGrid-item MuiGrid-grid-xs-12" style={{
              width:"100%",
              height:"150px",
              padding:"16px",
              backgroundColor:"#fff",
              flexGrow:0,
              maxWidth:"100%",
              boxSizing:"border-box",
              flexBasis:"100%",
              margin:0,
            }}>
              <div class="jss22" style={{
                width:"100%",
                display:"flex",
                flexDirection:"row",  
              }}>
                <div class="MuiGrid-root jss20 MuiGrid-item MuiGrid-grid-xs-9" style={{
                display:"flex",
                flexDirection:"column",
                justifyContent:"flex-end"
                }}>
                                  <div>
             <Typography style={{
               fontWeight:"bold",
               fontSize:"15px",
               color:"rgb(78, 83, 86)"
             }}>Kamu belanja di</Typography>
                </div>
                <div style={{
                  display:"flex",
                  flexDirection:"row",
                  marginTop:"6px",
                }}>
                  <img src="https://goodpangan.com//static/media/marketIcon.c968765c.svg">
                  </img>
                  <p class="MuiTypography-root MuiTypography-body1" style={{
                    fontWeight:"bold",
                    fontSize:"17px",
                    marginLeft:"6px"
                  }}>
                    Clutser Semarang
                  </p>
                </div>
                </div>
            <div class="MuiGrid-root jss21 MuiGrid-item MuiGrid-grid-xs-3" style={{
              display:"flex",
              justifyContent:"flex-end",
            flexDirection:"row",
              alignItems:"flex-end"
            }}>
              <div style={{
                display: "flex", alignitems: "center", cursor: "pointer",flexGrow:0,flexBasis:"25%",maxWidth:"25%"
              }}>
                <Typography onClick={()=>router.push("pasar")} class="MuiTypography-root MuiTypography-body1" style={{
                  width:"100%",
                  fontSize:"16px",
                  display:"flex",
                  justifyContent:"flex-end",
                  color:"rgb(47, 156, 241)",
                  marginLeft:"-20px"
                }}>
                  Ganti
                </Typography>
                <img src="https://goodpangan.com//static/media/change-loc.7755a7df.svg" onClick={()=>router.push("/pasar")}style={{
           marginLeft:"5px"
                }}></img>
              </div>
            </div>
               </div>
               <div class="jss23" style={{
                width:"100%",
                height:"40%",
                display:"flex",
                padding:"8px 16 px",
                marginTop:"16px",
                alignItems:"center",
               borderRadius:"50px",
                backgroundColor:"#F1F2F6",
               }}>
                 <div class="jss24" style={{
                   color:"#707585",
                   display:"flex",
                   position:"absolute",
                   alignItems:"center",
                   pointerEvents:"none",
                   justifyContent:"center"
                 }}>
     <svg class="MuiSvgIcon-root" style={{
       fill:"currentColor",
       width:"1.5rem",
       height:"1.5rem",
       display:"inline-block",
       marginLeft:"15px",
       fontSize:"1.5rem",
       transition:"fill 200ms cubic-bezier(0.4, 0, 0.2, 1) 0ms",
       flexShrink:0,
       userSelect:"none"
     }}>
       <path d="M15.5 14h-.79l-.28-.27C15.41 12.59 16 11.11 16 9.5 16 5.91 13.09 3 9.5
        3S3 5.91 3 9.5 5.91 16 9.5 16c1.61 0 3.09-.59 4.23-1.57l.27.28v.79l5 4.99L20.49
         19l-4.99-5zm-6 0C7.01 14 5 11.99 5 9.5S7.01 5 9.5 5 14 7.01 14 9.5 11.99 14 9.5 14z"></path>
     </svg>
                 </div>
                 <div class="MuiInputBase-root jss76" style={{
                   color:"inherit",
                   width:"100%",
                   height:"100%",
                   marginLeft:"32px",
                   cursor:"text",
                   display:"inline-flex",
                   position:"relative",
                   fontSize:"1rem",
                   boxSizing:"border-box",
                   alignItems:"center",
                   fontWeight:400,
                   lineHeight:"1.1876em",
                   
                 }}>
                   <input placeholder="Cari produk dan makanan terbaik..." type="text" class="MuiInputBase-input jss128" style={{
                     width:"100%",
                     fontSize:"14px",
                     font:"inherit",
                     color:"currentColor",
                     border:0,
                     height:"1.1876em",
                     margin:0,
                     display:"block",
                     padding:"6px 0 7px",
                     minWidth:0,
                     background:"none",
                     boxSizing:"content-box",
                     animationName:"mui-auto-fill-cancel",
                     letterSpacing:"inherit",
                     animationDuration:"10ms",
                     WebkitTapHighlightColor:"transparent",
                    marginLeft:"15px"
                   }}></input>
                 </div>
               </div>
              </div>
              </div>
              <br/>
              {/* <div class ="jss213" style={{
                marginTop:"143px",
                width:"100%",
                backgroundColor:"#fff",
                boxSizing:"inherit",
                display:"block",
                
              }}>
                <div class="MuiGrid-root jss231 MuiGrid-container MuiGrid-item MuiGrid-grid-xs-12" style={{
                  width:"100%",
                  display:"flex",
                  padding:"16px",
                  flexDirection:"column",
                  flexGrow:0,
                  maxWidth:"100%",
                  flexBasis:"100%",
                  margin:0,
                  boxSizing:"border-box",
                }}>
                  <Typography style={{
                    fontSize:"17px",
                    fontWeight:"bold"
                  }}>
              Promo Menarik Untukmu!
                  </Typography>
                  <Typography style={{
                    fontSize:"11px",
                    color:"rgb(82, 87, 92)"
                  }}>
              Banyak promo produk terbaik untukmu
                  </Typography>
                  </div>
                  </div> */}
                  <Promo/>
  <Category/>
  <div>
  <PilihanHemat/>

  </div>
  <div>
    <BottomNav/>
  </div>
  
                  </div>
                  </div>
   </div>
    )
}
export default AppBar;