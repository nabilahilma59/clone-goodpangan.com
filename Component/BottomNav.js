import React, {} from "react";
import BottomNavigation from "@material-ui/core/BottomNavigation";
import BottomNavigationAction from "@material-ui/core/BottomNavigationAction";
import HomeIcon from '@material-ui/icons/Home';
import ReceiptIcon from "@material-ui/icons/Receipt";
import LiveHelpIcon from "@material-ui/icons/LiveHelp";
import PersonIcon from "@material-ui/icons/Person";
import { useRouter} from "next/router"
import Grid from "@material-ui/core/Grid";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  actionItem: {
    "&$selected": {
      color: "#FF7632"
    }
  },
  selected: {}
}));

function BottomNav() {
  const router = useRouter()
  const classes = useStyles();
  const [value, setValue] = React.useState(0);
  return (
    <>
      <Grid
        container
        alignItems="center"
        justify="center"
        style={{
          height: "40px",
          width: "100%"
        }}
      >
        <BottomNavigation
          class="MuiBottomNavigation-root"
          style={{
            width: "440px",
            position: "fixed",
            boxShadow: "0px 0px 2px #9e9e9e",
            bottom: "0",
            fontFamily: "Montserrat"
          }}
          value={value}
          onChange={(event, newValue) => {
            setValue(newValue);
          }}
          showLabels
          className={classes.root}
        >
          <BottomNavigationAction style={{
            width:"300px"
          }}
            
            classes={{ root: classes.actionItem, selected: classes.selected }}
           
            label="Belanja"
            icon={<HomeIcon />}
          />
          <BottomNavigationAction
           
            classes={{ root: classes.actionItem, selected: classes.selected }}
            onClick={()=>router.push("/login?ref=/orders")}
         
            label="Transaksi"
            icon={<ReceiptIcon />}
          />
          <BottomNavigationAction
         
            classes={{ root: classes.actionItem, selected: classes.selected }}
            label="Bantuan"
            onClick={()=>router.push("/help")}
            icon={<LiveHelpIcon />}
          />
          <BottomNavigationAction
       
            classes={{ root: classes.actionItem, selected: classes.selected }}
            label="Profile"
            onClick={()=>router.push("/login?ref=/orders")}
            icon={<PersonIcon />}
          />
        </BottomNavigation>
      </Grid>
    </>
  );
}
export default BottomNav;
