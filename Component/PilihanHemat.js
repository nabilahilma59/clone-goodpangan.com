import { Typography } from "@material-ui/core";
import React from "react";
import { useState } from "react";
import { useRouter } from "next/router";
import ButtonProduk from "./ButtonProduk";
 
function PilihanHemat() {
  const router = useRouter();
  const [quantity, setQuantity] = useState(0);
  function tambah() {
    setQuantity(quantity + 1);
  }
  function kurang() {
    if (quantity > 0) {
      setQuantity(quantity - 1);
    }
  }
 
  return (
    <div>
      <div
        class="jss9"
        style={{
          margin: "8px 0 0",
          minHeight: "390px",
          paddingBottom: "16px",
          backgroundColor: "#fff",
        }}
      >
        <div
          class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-12"
          style={{
            display: "flex",
            flexdirection: "row",
            padding: "16px 16px 20px;",
            flexGrow: 0,
            maxWidth: "100%",
            flexBasis: "100%",
            margin: 0,
            boxSizing: "16px 16px 20px;",
          }}
        >
          <div
            class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-9"
            style={{
              flexGrow: 0,
              maxWidth: "75%",
              flexBasis: "100%",
              margin: 0,
              boxSizing: "border-box",
              overflowX: "hidden",
            }}
          >
            <Typography
              class="MuiTypography-root MuiTypography-body1"
              style={{
                fontSize: "15px",
                fontWeight: "bold",
                marginTop: "10px",
                marginLeft: "15px",
              }}
            >
              Pilihan Hemat Bulan ini
            </Typography>
            <Typography
              class="MuiTypography-root MuiTypography-body1"
              style={{
                fontSize: "12px",
                fontWeight: "bold",
                marginTop: "1px",
                marginLeft: "15px",
                color: "rgb(37, 37, 37)",
              }}
            >
              Diskon spesial untukmu yang spesial
            </Typography>
          </div>
          <div
            class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-3"
            style={{
              display: "flex",
              alignitems: "center",
              justifyContent: "flex-end",
            }}
          >
            <span
              onClick={() => router.push("/top-seller")}
              class="jss12"
              style={{
                fontSize: "14px",
                color: "#FF9059",
                cursor: "pointer",
                fontWeight: "bold",
                marginTop: "20px",
                marginRight: "17px",
              }}
            >
              Lihat semua
            </span>
          </div>
        </div>
        <div
          class="MuiGrid-root jss183"
          style={{
            height: "150%",
            display: "flex",
            padding: "7px 16px",
            flexWrap: "nowrap",
            overflowX: "hidden",
            backgroundColor: "#fff",
            marginTop: "20px",
            overflowX: "auto",
          }}
        >
          <div
            style={{
              cursor: "pointer",
              height: "300px",
              display: "flex",
              minWidth: "150px",
              boxShadow: "0px 4px 10px rgb(0 0 0 / 5%)",
              marginRight: "16px",
              borderRadius: "7px",
              flexDirection: "column",
            }}
          >
            <div
              class="MuiCardMedia-root jss209"
              style={{
                backgroundimage:
                  "https://dashboard.goodpangan.com/wp-content/uploads/2020/07/FB_IMG_1566134767733.jpg",
                width: "100%",
                height: "150px",
                objectFit: "cover",
                borderRadius: "7px 7px 0 0",
              }}
            ></div>
            <div
              style={{
                width: "100%",
                height: "150px",
                display: "flex",
                padding: "16px",
                objectFit: "cover",
                borderRadius: "0 0 7px 7px",
                flexDirection: "column",
                justifyContent: "space-between",
              }}
            >
              <div
                style={{
                  display: "flex",
                  flexDirection: "column",
                  justifyContent: "space-between",
                  height: "100%",
                }}
              >
                <span
                  class="jss213"
                  style={{
                    fontSize: "13px",
                    fontWeight: "bold",
                  }}
                >
                  Ain Drop II
                </span>
                <div
                  style={{
                    display: "flex",
                    flexDirection: "column",
                    justifyContent: "center",
                    height: "100%",
                  }}
                >
                  <span>
                    <span
                      class="jss215"
                      style={{
                        color: "#FF9059",
                        fontSize: "14px",
                        fontWeight: "bold",
                      }}
                    >
                      Rp 75.000
                    </span>
                  </span>
                </div>
              </div>
 
             <ButtonProduk/>
              </div>
          </div>
          <div
            class="jss208"
            style={{
              cursor: "pointer",
              height: "300px",
              display: "flex",
              minWidth: "150px",
              boxShadow: "0px 4px 10px rgb(0 0 0 / 5%)",
              marginRight: "16px",
              borderRadius: "7px",
              flexDirection: "column",
            }}
          >
            <div
              class="MuiCardMedia-root jss209"
              style={{
                backgroundImage:
                  "https://dashboard.goodpangan.com/wp-content/uploads/2020/07/images-3.jpg",
                width: "100%",
                height: "150px",
                objectFit: "cover",
                borderRadius: "7px 7px 0 0",
              }}
            ></div>
            <div
              class="jss212"
              style={{
                width: "100%",
                height: "150px",
                display: "flex",
                padding: "16px",
                objectFit: "cover",
                borderRadius: "0 0 7px 7px",
                flexDirection: "column",
                justifyContent: "space-between",
              }}
            >
              <div
                style={{
                  display: "flex",
                  flexDirection: "column",
                  justifyContent: "space-between",
                  height: "100%",
                }}
              >
                <span
                  class="jss213"
                  style={{
                    fontSize: "14px",
                    fontWeight: "bold",
                  }}
                >
                  Alpukat
                </span>
                <div
                  style={{
                    display: "flex",
                    flexDirection: "column",
                    justifyContent: "center",
                    height: "100%",
                  }}
                >
                  <span>
                    <span
                      class="jss215"
                      style={{
                        color: "#FF9059",
                        fontSize: "14px",
                        fontWeight: "bold",
                      }}
                    >
                      Rp 0
                    </span>
                  </span>
                </div>
              </div>
 
         
            <ButtonProduk/>
            </div>
          </div>
 
          <div
            class="jss208"
            style={{
              cursor: "pointer",
              height: "300px",
              display: "flex",
              minWidth: "150px",
              boxShadow: "0px 4px 10px rgb(0 0 0 / 5%)",
              marginRight: "16px",
              borderRadius: "7px",
              flexDirection: "column",
            }}
          >
            <div
              class="MuiCardMedia-root jss209"
              style={{
                backgroundImage:
                  "https://dashboard.goodpangan.com/wp-content/uploads/2020/07/images-3.jpg",
                width: "100%",
                height: "150px",
                objectFit: "cover",
                borderRadius: "7px 7px 0 0",
              }}
            ></div>
            <div
              class="jss212"
              style={{
                width: "100%",
                height: "150px",
                display: "flex",
                padding: "16px",
                objectFit: "cover",
                borderRadius: "0 0 7px 7px",
                flexDirection: "column",
                justifyContent: "space-between",
              }}
            >
              <div
                style={{
                  display: "flex",
                  flexDirection: "column",
                  justifyContent: "space-between",
                  height: "100%",
                }}
              >
                <span
                  class="jss213"
                  style={{
                    fontSize: "14px",
                    fontWeight: "bold",
                  }}
                >
                  Alpukat
                </span>
                <div
                  style={{
                    display: "flex",
                    flexDirection: "column",
                    justifyContent: "center",
                    height: "100%",
                  }}
                >
                  <span>
                    <span
                      class="jss215"
                      style={{
                        color: "#FF9059",
                        fontSize: "14px",
                        fontWeight: "bold",
                      }}
                    >
                      Rp 0
                    </span>
                  </span>
                </div>
              </div>
             <ButtonProduk/>
              </div>
          </div>
          <div
            class="jss208"
            style={{
              cursor: "pointer",
              height: "300px",
              display: "flex",
              minWidth: "150px",
              boxShadow: "0px 4px 10px rgb(0 0 0 / 5%)",
              marginRight: "16px",
              borderRadius: "7px",
              flexDirection: "column",
            }}
          >
            <div
              class="MuiCardMedia-root jss209"
              style={{
                backgroundImage:
                  "https://dashboard.goodpangan.com/wp-content/uploads/2020/07/fix.am1_.png",
                width: "100%",
                height: "150px",
                objectFit: "cover",
                borderRadius: "7px 7px 0 0",
              }}
            ></div>
            <div
              class="jss212"
              style={{
                width: "100%",
                height: "150px",
                display: "flex",
                padding: "16px",
                objectFit: "cover",
                borderRadius: "0 0 7px 7px",
                flexDirection: "column",
                justifyContent: "space-between",
              }}
            >
              <div
                style={{
                  display: "flex",
                  flexDirection: "column",
                  justifyContent: "space-between",
                  height: "100%",
                }}
              >
                <span
                  class="jss213"
                  style={{
                    fontSize: "14px",
                    fontWeight: "bold",
                  }}
                >
                  AM-I
                </span>
                <div
                  style={{
                    display: "flex",
                    flexDirection: "column",
                    justifyContent: "center",
                    height: "100%",
                  }}
                >
                  <span>
                    <span
                      class="jss215"
                      style={{
                        color: "#FF9059",
                        fontSize: "14px",
                        fontWeight: "bold",
                      }}
                    >
                      Rp 350.000
                    </span>
                  </span>
                </div>
              </div>
            <ButtonProduk/>
              </div>
          </div>
          <div
            class="jss208"
            style={{
              cursor: "pointer",
              height: "300px",
              display: "flex",
              minWidth: "150px",
              boxShadow: "0px 4px 10px rgb(0 0 0 / 5%)",
              marginRight: "16px",
              borderRadius: "7px",
              flexDirection: "column",
            }}
          >
            <div
              class="MuiCardMedia-root jss209"
              style={{
                backgroundImage:
                  "https://dashboard.goodpangan.com/wp-content/uploads/2020/07/fix.baiduri.png",
                width: "100%",
                height: "150px",
                objectFit: "cover",
                borderRadius: "7px 7px 0 0",
              }}
            ></div>
            <div
              class="jss212"
              style={{
                width: "100%",
                height: "150px",
                display: "flex",
                padding: "16px",
                objectFit: "cover",
                borderRadius: "0 0 7px 7px",
                flexDirection: "column",
                justifyContent: "space-between",
              }}
            >
              <div
                style={{
                  display: "flex",
                  flexDirection: "column",
                  justifyContent: "space-between",
                  height: "100%",
                }}
              >
                <span
                  class="jss213"
                  style={{
                    fontSize: "14px",
                    fontWeight: "bold",
                  }}
                >
                  Baiduri Pak Haji
                </span>
                <div
                  style={{
                    display: "flex",
                    flexDirection: "column",
                    justifyContent: "center",
                    height: "100%",
                  }}
                >
                  <span>
                    <span
                      class="jss215"
                      style={{
                        color: "#FF9059",
                        fontSize: "14px",
                        fontWeight: "bold",
                      }}
                    >
                      Rp 130.000
                    </span>
                  </span>
                </div>
              </div>
             <ButtonProduk/>
              </div>
          </div>
          <div
            class="jss125"
            style={{
              cursor: "pointer",
              height: "300px",
              display: "flex",
              minWidth: "150px",
              boxShadow: "0px 4px 10px rgb(0 0 0 / 5%)",
              marginRight: "16px",
              borderRadius: "7px",
              flexDirection: "column",
            }}
          >
            <div
              class="MuiCardMedia-root jss126"
              style={{
                backgroundImage:
                  "https://dashboard.goodpangan.com/wp-content/uploads/2020/07/5ad9b2e8f47c95e6c32f03bb4a9e67f6.jpg",
                width: "100%",
                height: "150px",
                objectFit: "cover",
                borderRadius: "7px 7px 0 0",
              }}
            ></div>
            <div
              class="jss129"
              style={{
                width: "100%",
                height: "150px",
                display: "flex",
                padding: "16px",
                objectFit: "cover",
                borderRadius: "0 0 7px 7px",
                flexDirection: "column",
                justifyContent: "space-between",
              }}
            >
              <div
                style={{
                  display: "flex",
                  flexDirection: "column",
                  justifyContent: "space-between",
                  height: "100%",
                }}
              >
                <span
                  class="jss213"
                  style={{
                    fontSize: "14px",
                    fontWeight: "bold",
                  }}
                >
                  Bedak Nur
                </span>
                <div
                  style={{
                    display: "flex",
                    flexDirection: "column",
                    justifyContent: "center",
                    height: "100%",
                  }}
                >
                  <span>
                    <span
                      class="jss215"
                      style={{
                        color: "#FF9059",
                        fontSize: "14px",
                        fontWeight: "bold",
                      }}
                    >
                      Rp 130.000
                    </span>
                  </span>
                </div>
              </div>
            <ButtonProduk/>
            
            </div>
          </div>
          <div
            class="jss125"
            style={{
              cursor: "pointer",
              height: "300px",
              display: "flex",
              minWidth: "150px",
              boxShadow: "0px 4px 10px rgb(0 0 0 / 5%)",
              marginRight: "16px",
              borderRadius: "7px",
              flexDirection: "column",
            }}
          >
            <div
              class="MuiCardMedia-root jss126"
              style={{
                backgroundImage:
                  "https://dashboard.goodpangan.com/wp-content/uploads/2020/07/Bedak-Talc.png",
                width: "100%",
                height: "150px",
                objectFit: "cover",
                borderRadius: "7px 7px 0 0",
              }}
            ></div>
            <div
              class="jss129"
              style={{
                width: "100%",
                height: "150px",
                display: "flex",
                padding: "16px",
                objectFit: "cover",
                borderRadius: "0 0 7px 7px",
                flexDirection: "column",
                justifyContent: "space-between",
              }}
            >
              <div
                style={{
                  display: "flex",
                  flexDirection: "column",
                  justifyContent: "space-between",
                  height: "100%",
                }}
              >
                <span
                  class="jss213"
                  style={{
                    fontSize: "14px",
                    fontWeight: "bold",
                  }}
                >
                  Bedak Talcum Family
                </span>
                <div
                  style={{
                    display: "flex",
                    flexDirection: "column",
                    justifyContent: "center",
                    height: "100%",
                  }}
                >
                  <span>
                    <span
                      class="jss215"
                      style={{
                        color: "#FF9059",
                        fontSize: "14px",
                        fontWeight: "bold",
                      }}
                    >
                      Rp 22.000
                    </span>
                  </span>
                </div>
              </div>
              <ButtonProduk/>
              </div>
          </div>
          <div
            class="jss125"
            style={{
              cursor: "pointer",
              height: "300px",
              display: "flex",
              minWidth: "150px",
              boxShadow: "0px 4px 10px rgb(0 0 0 / 5%)",
              marginRight: "16px",
              borderRadius: "7px",
              flexDirection: "column",
            }}
          >
            <div
              class="MuiCardMedia-root jss126"
              style={{
                backgroundImage:
                  "https://dashboard.goodpangan.com/wp-content/uploads/2020/07/Bedak-Talc.png",
                width: "100%",
                height: "150px",
                objectFit: "cover",
                borderRadius: "7px 7px 0 0",
              }}
            ></div>
            <div
              class="jss129"
              style={{
                width: "100%",
                height: "150px",
                display: "flex",
                padding: "16px",
                objectFit: "cover",
                borderRadius: "0 0 7px 7px",
                flexDirection: "column",
                justifyContent: "space-between",
              }}
            >
              <div
                style={{
                  display: "flex",
                  flexDirection: "column",
                  justifyContent: "space-between",
                  height: "100%",
                }}
              >
                <span
                  class="jss213"
                  style={{
                    fontSize: "14px",
                    fontWeight: "bold",
                  }}
                >
                  Benih Kacang Ijo
                </span>
                <div
                  style={{
                    display: "flex",
                    flexDirection: "column",
                    justifyContent: "center",
                    height: "100%",
                  }}
                >
                  <span>
                    <span
                      class="jss215"
                      style={{
                        color: "#FF9059",
                        fontSize: "14px",
                        fontWeight: "bold",
                      }}
                    >
                      Rp 25.000
                    </span>
                  </span>
                </div>
              </div>
              <ButtonProduk/>
            </div>
          </div>
          <div
            class="jss125"
            style={{
              cursor: "pointer",
              height: "300px",
              display: "flex",
              minWidth: "150px",
              boxShadow: "0px 4px 10px rgb(0 0 0 / 5%)",
              marginRight: "16px",
              borderRadius: "7px",
              flexDirection: "column",
            }}
          >
            <div
              class="MuiCardMedia-root jss126"
              style={{
                backgroundImage:
                  "https://dashboard.goodpangan.com/wp-content/uploads/2020/07/5ad9b2e8f47c95e6c32f03bb4a9e67f6.jpg",
                width: "100%",
                height: "150px",
                objectFit: "cover",
                borderRadius: "7px 7px 0 0",
              }}
            ></div>
            <div
              class="jss129"
              style={{
                width: "100%",
                height: "150px",
                display: "flex",
                padding: "16px",
                objectFit: "cover",
                borderRadius: "0 0 7px 7px",
                flexDirection: "column",
                justifyContent: "space-between",
              }}
            >
              <div
                style={{
                  display: "flex",
                  flexDirection: "column",
                  justifyContent: "space-between",
                  height: "100%",
                }}
              >
                <span
                  class="jss213"
                  style={{
                    fontSize: "14px",
                    fontWeight: "bold",
                  }}
                >
                  Beras
                </span>
                <div
                  style={{
                    display: "flex",
                    flexDirection: "column",
                    justifyContent: "center",
                    height: "100%",
                  }}
                >
                  <span>
                    <span
                      class="jss215"
                      style={{
                        color: "#FF9059",
                        fontSize: "14px",
                        fontWeight: "bold",
                      }}
                    >
                      Rp 25.000
                    </span>
                  </span>
                </div>
              </div>
              <ButtonProduk/>
              </div>
          </div>
          <div
            class="jss125"
            style={{
              cursor: "pointer",
              height: "300px",
              display: "flex",
              minWidth: "150px",
              boxShadow: "0px 4px 10px rgb(0 0 0 / 5%)",
              marginRight: "16px",
              borderRadius: "7px",
              flexDirection: "column",
            }}
          >
            <div
              class="MuiCardMedia-root jss126"
              style={{
                backgroundImage:
                  "https://dashboard.goodpangan.com/wp-content/uploads/2020/07/5ad9b2e8f47c95e6c32f03bb4a9e67f6.jpg",
                width: "100%",
                height: "150px",
                objectFit: "cover",
                borderRadius: "7px 7px 0 0",
              }}
            ></div>
            <div
              class="jss129"
              style={{
                width: "100%",
                height: "150px",
                display: "flex",
                padding: "16px",
                objectFit: "cover",
                borderRadius: "0 0 7px 7px",
                flexDirection: "column",
                justifyContent: "space-between",
              }}
            >
              <div
                style={{
                  display: "flex",
                  flexDirection: "column",
                  justifyContent: "space-between",
                  height: "100%",
                }}
              >
                <span
                  class="jss213"
                  style={{
                    fontSize: "14px",
                    fontWeight: "bold",
                  }}
                >
                  Beras Hitam
                </span>
                <div
                  style={{
                    display: "flex",
                    flexDirection: "column",
                    justifyContent: "center",
                    height: "100%",
                  }}
                >
                  <span>
                    <span
                      class="jss215"
                      style={{
                        color: "#FF9059",
                        fontSize: "14px",
                        fontWeight: "bold",
                      }}
                    >
                      Rp 8.800
                    </span>
                  </span>
                </div>
              </div>
              <ButtonProduk/>
              </div>
          </div>
        </div>
      </div>
    </div>
  );
}
export default PilihanHemat;