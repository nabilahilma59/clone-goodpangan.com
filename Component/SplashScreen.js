import React, {useEffect} from "react";
import Grid from "@material-ui/core/Grid" 
import Container from '@material-ui/core/Container';
import CircularProgress from '@material-ui/core/CircularProgress';


function SplashScreen() {

    return(
      <Container maxWidth="sm">
        <Grid container alignItems="center" justify="center">
       <div style={{
           width:"100%",
       }}>
<div style={{width: "100%", display:"flex", justifyContent: "flex-start"}}>

<img
    src="https://goodpangan.com/static/media/logo.e39a3ecd.svg" 
    style={{
    width:150,
    marginTop:"10%",
    marginBottom:"3%",
    marginLeft:"13%",
    display:"flex",
    alignItems:"end",
    }}>
    </img>
    
    
</div>

<div>
    <Grid container alignItems="center" justify="center" >
       
    <img
    
    src="https://goodpangan.com/static/media/splash.62c8e392.svg"
    style={{
     width:400,
     marginTop:"23%",
     position:"fixed"
    }}>
        
    </img>
    
    </Grid>
    
</div>

</div>
<div style={{
    marginTop:"30%",
    }}
    >
        <CircularProgress style={{
            color:"#fff"
        }}/>
       
    </div>
       </Grid>
</Container>
    );
}
export default SplashScreen