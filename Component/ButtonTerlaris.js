import { useState } from "react";
import Button from "@material-ui/core/Button"

function ButtonTerlaris(params) {
    
  const [quantity, setQuantity] = useState(0);
  function tambah() {
    setQuantity(quantity + 1);
  }
  function kurang() {
    if (quantity > 0) {
      setQuantity(quantity - 1);
    }
  }
    return(
<div>
    <div>
    {quantity === 0 && (
                <button
                  onClick={tambah}
                  class="MuiButtonBase-root MuiButton-root MuiButton-text jss217"
                  tabIndex="0"
                  type="button"
                  style={{
                    color:"rgb(255, 255, 255)",
                    backgroundColor:"rgb(255, 118, 50)",
                    borderRadius:"4px",
                    width:"90px",
                    height:"30px",
                    fontSize:"10px",
                    textTransform:"none",
                    padding:"6px 8px",
                    minWidth:"64px",
                    boxSizing:"border-box",
                    transition:"background-color 250ms cubic-bezier(0.4, 0, 0.2, 1) 0ms,box-shadow 250ms cubic-bezier(0.4, 0, 0.2, 1) 0ms,border 250ms cubic-bezier(0.4, 0, 0.2, 1) 0ms"
                  }}
                >
                  <span
                    class="MuiButton-label"
                    style={{
                      width: "100%",
                      display: "inherit",
                      alignItems: "inherit",
                      justifyContent: "inherit",
                    }}
                  >
                    <b>Tambahkan</b>
                  </span>
                  <span
                    class="MuiTouchRipple-root"
                    style={{
                      top: 0,
                      bottom: 0,
                      right: 0,
                      left: 0,
                      zIndex: 0,
                      overflow: "hidden",
                      position: "absolute",
                      borderRadius: "inherit",
                      pointerEvents: "none",
                    }}
                  ></span>
                </button>
              )}
              <div
                style={{
                  display: "flex",
                  justifyContent: "space-between",
                  alignItems: "center",
                }}
              >
                {quantity > 0 && (
                  <>
                    <button
                      onClick={kurang}
                      style={{
                        color: "rgb(21, 59, 80)",
                        borderRadius: "4px",
                        minHeight:"30px",
                        minWidth:"30px",
                        padding: "1px",
                        border: "1px solid silver",
                      }}
                    >
                      <span
                        class="MuiButton-label"
                        style={{
                          width: "100%",
                          display: "inherit",
                          justifyContent: "inherit",
                          alignItems: "inherit",
                        }}
                      >
                        -
                      </span>
                      <span
                        class="MuiTouchRipple-root"
                        style={{
                          top: 0,
                          left: 0,
                          right: 0,
                          bottom: 0,
                          zIndex: 0,
                          overflow: "hidden",
                          position: "absolute",
                          borderRadius: "inherit",
                          pointerEvents: "none",
                        }}
                      ></span>
                    </button>
                    <b> {quantity} </b>
                    <button
                      onClick={tambah}
                      style={{
                        color: "rgb(255, 255, 255)",
                        backgroundColor: "rgb(255, 118, 50)",
                        border: "1px solid rgb(255, 118, 50)",
                        borderRadius: "4px",
                      minHeight:"30px",
                     minWidth:"30px",
                        marginLeft: "-1px",
                      }}
                    >
                      <span
                        class="MuiButton-label"
                        style={{
                          width: "100%",
                          display: "inherit",
                          justifyContent: "inherit",
                          alignItems: "inherit",
                        }}
                      >
                        +
                      </span>
                      <span
                        class="MuiTouchRipple-root"
                        style={{
                          top: 0,
                          left: 0,
                          right: 0,
                          bottom: 0,
                          zIndex: 0,
                          overflow: "hidden",
                          position: "absolute",
                          borderRadius: "inherit",
                          pointerEvents: "none",
                        }}
                      ></span>
                    </button>
                  </>
                )}
              </div>
    </div>
</div>
    )
}
export default ButtonTerlaris;